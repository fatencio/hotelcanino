﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.plugins.setLang('wordcount', 'en', {
    WordCount: 'Palabras:',
    CharCount: 'Characters:',
    CharCountWithHTML: 'Characters (with HTML):',
    Paragraphs: 'Paragraphs:',
    //pasteWarning: 'Content cannot be pasted because it is above the allowed limit',
    pasteWarning: 'El contenido no se puede pegar, ya que se encuentra fuera del límite permitido',
    Selected: 'Selected: ',
    title: 'Statistics'
});
