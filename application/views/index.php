<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Finca Familia Acosta - Gestión de Reservas</title>

        <meta name="description" content="Finca Familia Acosta - Gestión de Reservas 1.0">
        <meta name="author" content="Francisco Atencio">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon.png">

        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-192x192.png" sizes="192x192">

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/slick/slick.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/slick/slick-theme.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/buttons.dataTables.min.css">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="<?php echo BASE_PATH ?>/assets/css/oneui.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" id="css-geotienda" href="<?php echo BASE_PATH ?>/assets/custom/css/geotienda.css">

        <!-- full calendar -->
        <link href='<?php echo BASE_PATH ?>/assets/plugins/fullcalendar/fullcalendar.min.css?<?php echo time();?>' rel='stylesheet'>
        <link href='<?php echo BASE_PATH ?>/assets/plugins/fullcalendar/fullcalendar.print.min.css?<?php echo time();?>' rel='stylesheet' media='print'>
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/custom/css/scheduler.min.css"> 

        <!--
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/custom/css/fullcalendar.min.css">        
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/custom/css/scheduler.min.css">        
        -->

        <!-- Custom plugins CSS -->
        <link rel="stylesheet" id="css-spectrum" href="<?php echo BASE_PATH ?>/assets/custom/css/spectrum.css">        

        <!-- END Stylesheets -->

        <!-- Librerias Hotel Canino v3 -->    
        <?php include 'librerias_js.php'; ?>    

         <!-- date picker -->
         <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">



    </head>
    <body>  
        <div id="page-loader"></div>
        <!-- Page Container -->
        <!--
            Available Classes:

            'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

            'sidebar-l'                  Left Sidebar and right Side Overlay
            'sidebar-r'                  Right Sidebar and left Side Overlay
            'sidebar-mini'               Mini hoverable Sidebar (> 991px)
            'sidebar-o'                  Visible Sidebar by default (> 991px)
            'sidebar-o-xs'               Visible Sidebar by default (< 992px)

            'side-overlay-hover'         Hoverable Side Overlay (> 991px)
            'side-overlay-o'             Visible Side Overlay by default (> 991px)

            'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

            'header-navbar-fixed'        Enables fixed header
        -->

        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

            <!-- Menu Principal -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            <a class="h5 text-white" href="<?php echo BASE_PATH ?>/">
                                <span class="h5 font-w600 sidebar-mini-hide">Gestión de Reservas</span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                               
                               <!-- RESERVAS -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('Reserva/index');"><i class="si si-notebook"></i><span class="sidebar-mini-hide">Reservas</span></a>
                                </li>   

                                <!-- CLIENTES -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('Cliente/index');"><i class="si si-users"></i><span class="sidebar-mini-hide">Clientes</span></a>
                                </li>    

                                <!-- MASCOTAS -->
                                <li>
                                    <a style="padding-top: 0 !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('Mascota/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Mascotas</span></a>
                                </li>    
                                <li style="height:6px;">

                                <!-- AREAS -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('Area/index');"><i class="si si-home"></i><span class="sidebar-mini-hide">Areas / Boxes</span></a>
                                </li>    
                                <li style="height:6px;">

                                <!-- ANIMALES -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('Animal/index');"><i class="fa fa-paw"></i><span class="sidebar-mini-hide">Animales</span></a>
                                </li>   
                                <li>
                                    <a style="padding-top: 0 !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('Tamanio/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Tamaños</span></a>
                                </li>   
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('Raza/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Razas</span></a>
                                </li>                                                

                                <li style="height:6px;">
                                
                                <!-- ADMINISTRADORES -->
                                <?php 
                                if ($is_admin){
                                ?>
                                    <li>
                                        <a href="javascript:void(0);" onclick="return loadController('Usuario/index');"><i class="si si-user"></i><span class="sidebar-mini-hide">Administradores</span></a>
                                    </li>  
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Menu Principal -->

            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">

                <!-- Nro de notificaciones nuevas -->
                <span class="badge badge-danger pull-right count-notificaciones" id="count_notificaciones"></span>

                <!-- Header Navigation Right -->
                <ul class="nav-header pull-right">

                    <!-- Usuario -->
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                                <?php echo $usuario ?> 
                                <img src="<?php echo BASE_PATH ?>/assets/img/backend/admin_avatar.jpg" alt="Avatar">

                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a tabindex="-1" href="javascript:void(0);" onclick="return loadController('Inicio/logout');">
                                        <i class="si si-logout pull-right"></i>Salir
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- FIN Usuario -->

                </ul>
                <!-- END Header Navigation Right -->

                <!-- Header Navigation Left -->
                <ul class="nav-header pull-left">
                    <li class="hidden-md hidden-lg">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                            <i class="fa fa-navicon"></i>
                        </button>
                        <a class="text-black" href="index.php" style="float:right; padding-left: 6px; font-size: 12px;">
                            <span >Gestión de Reservas</span>
                        </a>                             
                    </li>                 
                    <li class="hidden-xs hidden-sm">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                    </li>




                </ul>
                <!-- END Header Navigation Left -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-right">
                    Desarrollado por <a class="font-w600" href="" target="_blank">Francisco Atencio</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="https://goo.gl/6LF10W" target="_blank">Finca Familia Acosta - Gestión de Reservas 1.0</a> &copy; <span class="js-year-copy"></span>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Error no manejado Modal -->
        <div class="modal fade" id="modal_error_sistema" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h3 class="text-center text-naranja">Se ha producido un error</h3>
                            <br/>
                            <h4 class="font-w300 text-center">Por favor, intentá la operación nuevamente.</h4>
                            <br/><br/>

                            <div class="text-center">
                                <button class="btn btn-lg btn-geotienda" type="button" data-dismiss="modal">Aceptar</button>
                            </div>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- FIN Error no manejado Modal -->

        <!-- MODAL RESULTADO OK -->
        <div class="modal fade" id="modal_resultado_ok" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h3 class="font-w300 text-center resaltado-naranja" id="resultado_ok_titulo1"></h3>
                            <br/><br/>
                            <h4 class="font-w300 text-center" id="resultado_ok_titulo2"></h4>
                            <br/><br/>
                            </hr>
                            <div class="text-center">
                                <button class="btn btn-lg btn-geotienda" type="button" data-dismiss="modal">Aceptar</button>
                            </div>
                            <br/>

                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- fin MODAL RESULTADO OK -->   

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.scrollLock.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.appear.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.countTo.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.placeholder.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/js.cookie.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/app.js"></script>

        <!-- Page Plugins -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/slick/slick.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/chartjs/Chart.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <!--<script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/buttons.flash.min.js"></script>-->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/buttons.html5.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/jszip.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

        <!-- Custom Plugins -->
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.chained.remote.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.chained.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.cascadingdropdown.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/spectrum.js"></script>    
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/bootstrap-switch.js"></script>    
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyC0mza7rGNlOmcKg-5JF1NB3nUKqHFYMBk&sensor=false&v=3&libraries=geometry"></script>

        <!-- full calendar plugin -->
        <script src='<?php echo BASE_PATH ?>/assets/plugins/fullcalendar/moment.js'></script>
        <script src='<?php echo BASE_PATH ?>/assets/plugins/fullcalendar/fullcalendar.js'></script>
        <script src='<?php echo BASE_PATH ?>/assets/plugins/fullcalendar/scheduler.min.js'></script>        
        <script src='<?php echo BASE_PATH ?>/assets/plugins/fullcalendar/locale/es.js'></script>

        <!--
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/custom/js/fullcalendar.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/custom/js/gcal.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/custom/js/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/custom/js/scheduler.min.js"></script>
        -->

        <!-- Page JS Code -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/pages/base_tables_datatables.js"></script> 

        <script type="text/javascript">

        $(document).ready(function() 
        {
            // Carga inicialmente lista de Locales
            loadController('Reserva/index');
        });


        






        /* AVISOS notify */
        function aviso(tipo, mensaje, titulo){

            var icono;

            switch(tipo){
                case 'success':
                    icono = 'fa fa-check';
                break;

               case 'danger':
                    icono = 'fa fa-times';
                break;                
            }

            $.notify({
                // options
                icon: icono,
                title: titulo,
                message: mensaje 
            },{
                // settings
                type: tipo,
                placement: {
                    from: "top",
                    align: "center"
                },    
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<span data-notify="title" style="margin-bottom:8px;" class="clearfix">{1}</span> ' +
                    '<span data-notify="icon"></span> ' +                    
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'                     
            });   
        }
        /* FIN AVISOS notify */

        /* Refresco invisible de la sesion */
        var refreshTime = 300000; // cada 5 minutos
        setInterval( function() {
            $.ajax({
                cache: false,
                type: "GET",
                url: "../refreshSession.php",
                success: function(data) {
                }
            });
        }, refreshTime );        
        </script>     


        

        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>


    </body>
</html>





