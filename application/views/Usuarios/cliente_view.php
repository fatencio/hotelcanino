<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7"> 
            <h1 class="page-heading">
                Clientes
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('Cliente/index');">Clientes</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_cliente()"><i class="glyphicon glyphicon-plus"></i> Nuevo Cliente</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Teléfono</th>
                        <th>EMail</th>
                        <th style="min-width:156px;">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 25,        

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Cliente/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false,
        },    
        { 
            "targets": [ 4 ], 
            "className": "hidden-xs hidden-sm hidden-md",       
        },              
        {
            "targets": [2,3],
            "className": "hidden-xs",       
        }  
        ],

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_cliente()
{
    save_method = 'add';

    $('#form')[0].reset(); // reset form on modals

    $("#provincia").val(13);
    $('[id="provincia"]').attr('disabled',true); //set provincia disable 


    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nuevo Cliente'); // Set Title to Bootstrap modal title
}


function edit_cliente(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('[id="provincia"]').attr('disabled',true); //set provincia disable 
  

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/Cliente/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="apellido"]').val(data.apellido);
            $('[name="telefono"]').val(data.telefono);
            $('[name="email"]').val(data.email);
            $('[name="direccion"]').val(data.direccion);
            $('[name="localidad"]').val(data.localidad);
            
            if (data.departamento_id > 0) {
             $('[name="departamento"]').val(data.departamento_id);
            }
            else {
             $('[name="departamento"]').val(""); 
            }

            $('[name="provincia"]').val(data.provincia_id);
            $('[name="dni"]').val(data.dni);
            $('[name="telefono2"]').val(data.telefono2);
            $('[name="notas"]').val(data.notas);  

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Cliente'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/Cliente/ajax_add";
         mensaje = 'Cliente creado.';

    } else {
        url = "<?php echo BASE_PATH ?>/Cliente/ajax_update";
         mensaje = 'Cliente modificado.';
    }


    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form'));

    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,           
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', mensaje); 
            }
            else
            {
                // Sesion expirada
                if (data.login) 
                {
                    window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                }
                else
                {                
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Cliente (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_cliente(nombre, id)
{
    if(confirm('¿Eliminar Cliente "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Cliente/ajax_delete_full/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {   
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'Cliente eliminado.');                 
                }
                else
                {
                    // Sesion expirada
                    if (data.login) 
                    {
                        window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                    }
                    else
                    {
                        aviso('danger', data.mensaje, "ERROR AL ELIMINAR CLIENTE");  
                        console.log(data.error);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                 aviso('danger', textStatus, "ERROR ELIMINANDO DATOS CLIENTE");  
                 console.log(textStatus);
            }
        });

    }
}





function switch_cliente(id){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Cliente/ajax_activar_desactivar/" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            if (data.status)
            {
                if (data.nuevo_status == 1)
                    aviso('success', 'Cliente habliitado.');  
                else
                    aviso('success', 'Cliente deshabliitado.');  
            }
            else
            {
                // Sesion expirada
                if (data.login) window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {   
            console.log("errorThrown: " + errorThrown);
            aviso('danger', textStatus, 'Error al activar / desactivar.'); 
        }
    });

}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Cliente Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- APELLIDO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido</label>
                            <div class="col-md-9">
                                <input name="apellido" placeholder="Apellido" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                         <!-- DNI --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Dni</label>
                            <div class="col-md-9">
                                <input name="dni" placeholder="Dni" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- EMAIL --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">E-Mail</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="E-Mail" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- TELEFONO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono</label>
                            <div class="col-md-9">
                                <input name="telefono" placeholder="Teléfono" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                           <!-- TELEFONO2 --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono 2</label>
                            <div class="col-md-9">
                                <input name="telefono2" placeholder="Teléfono2" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <!-- DIRECCION --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Dirección</label>
                            <div class="col-md-4">
                                <input name="direccion" placeholder="Calle y Número" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                            
                            <div class="col-md-5">
                                <input name="localidad" placeholder="Ingrese Distrito/Localidad" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-4">
                                <select name="departamento" id="departamento" class="form-control" >
                                 
                                    <option selected value="">Seleccione departamento...</option> 
                                    <?php foreach($departamentos as $departamento){ ?>
                                        <option value="<?php echo $departamento->id ?>" ><?php echo $departamento->nombre ?></option> 
                                    <?php } ?>    
                                </select>  
                   
                                <span class="help-block"></span>
                            </div>  


                            <div class="col-md-5">
                                <select name="provincia" id="provincia" class="form-control" >
                                 
                                    <option selected value="">Seleccione provincia...</option> 
                                    <?php foreach($provincias as $provincia){ ?>
                                        <option value="<?php echo $provincia->id ?>" ><?php echo $provincia->nombre ?></option> 
                                    <?php } ?>    
                                </select>  
                   
                                <span class="help-block"></span>
                            </div>                                                     
                        </div>


                        <!-- NOTAS --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Notas</label>
                            <div class="col-md-9">
                               <textarea class="form-control" name="notas" id="notas" rows="3"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                     
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->     