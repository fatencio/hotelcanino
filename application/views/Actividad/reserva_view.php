<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7"> 
            <h1 class="page-heading">
                Reservas
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('reserva/index');">Reservas</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_reserva()"><i class="glyphicon glyphicon-plus"></i> Nueva reserva</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <div id='calendar'></div>
        </div>   
    </div>         
</div>

<script>

var save_method; //for save method string
var table;

$(document).ready(function() 
{
    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });


    jQuery('.js-datepicker').add('.input-daterange').datepicker({
        weekStart: 1,
        autoclose: true,
        todayHighlight: true
    });

    $('#calendar').fullCalendar(
    {
          editable: false,
          aspectRatio: 1.8,
          //scrollTime: '00:00',
          header: {
            left: 'today prev,next',
            center: 'title',
            right: 'timelineTwoWeek,timelineMonth,timelineThreeMonth'
          },
          defaultView: 'timelineTwoWeek',
          views: {     
            timelineTwoWeek: {
                type: 'timeline',
                duration: { week: 2 },
                slotDuration: { days:1 }
            }, 
            timelineThreeMonth: {
                type: 'timeline',
                duration: { months: 3 },
                slotDuration: { days:1 }
            },             
          },
          navLinks: false,
          resourceAreaWidth: '15%',
          resourceLabelText: 'Area',

          resources: 
          {
              url: BASE_PATH + '/Reserva/ajax_areas',
              type: 'POST', 
              error: function() {
                  alert('Error al cargar áreas. Por favor recargue la página.');
                }
          },
          events: 
          {
              url: BASE_PATH + '/Reserva/ajax_reservas',
              type: 'POST', 
              error: function() {
                  alert('Error al cargar reservas. Por favor recargue la página.');
                }
          },   

          eventClick: function(calEvent, jsEvent, view) 
          {
                edit_reserva(calEvent.id)

                /*
                alert('Event: ' + calEvent.title);
                alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                alert('View: ' + view.name);
                */
                return false;
          },              

          schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source'
    });

});


$(function()
{
    $("#mascota_id").chained("#animal_id");
    $("#box_id").chained("#area_id");
});

function add_reserva()
{
    save_method = 'add';

    $('#form')[0].reset(); // reset form on modals

    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nueva reserva'); // Set Title to Bootstrap modal title
}




function edit_reserva(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/reserva/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);

            $('[name="animal_id"]').val(data.animal_id).change();
            $('[name="mascota_id"]').val(data.mascota_id);
            
            $('[name="ingreso"]').val(data.fecha_desde);
            $('[name="egreso"]').val(data.fecha_hasta);
            
            $('[name="area_id"]').val(data.area_id).change();
            $('[name="box_id"]').val(data.box_id);

            $('[name="notas"]').val(data.notas);  

            $('#modal_form').modal('show'); 
            $('.modal-title').text('Editar reserva');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/reserva/ajax_add";
         mensaje = 'Reserva creada.';

    } else {
        url = "<?php echo BASE_PATH ?>/reserva/ajax_update";
         mensaje = 'Reserva modificada.';
    }

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                $('#modal_form').modal('hide');
                aviso('success', mensaje); 

                $('#calendar').fullCalendar( 'refetchEvents' )
            }
            else
            {
                // Sesion expirada
                if (data.login) 
                {
                    window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                }
                else
                {                
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar reserva (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


function delete_reserva(nombre, id)
{
    if(confirm('¿Eliminar reserva "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/reserva/ajax_delete_full/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {   
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'reserva eliminada.');                 
                }
                else
                {
                    // Sesion expirada
                    if (data.login) 
                    {
                        window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                    }
                    else
                    {
                        aviso('danger', data.mensaje, "ERROR AL ELIMINAR reserva");  
                        console.log(data.error);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                 aviso('danger', textStatus, "ERROR ELIMINANDO DATOS reserva");  
                 console.log(textStatus);
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">reserva Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <input type="hidden" value="1" name="estado"/> 
                    <div class="form-body">

                        <!-- ANIMAL --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Animal</label>
                            <div class="col-md-9">
                                   <select name="animal_id" id="animal_id" class="form-control" >
                                   <option selected value="" >Seleccione tipo de animal...</option> 
                                    <?php foreach($animales as $animal){ ?>
                                        <option value="<?php echo $animal->id ?>" ><?php echo $animal->nombre ?></option> 
                                    <?php } ?>    
                                    </select> 
                                <span class="help-block"></span>
                            </div>
                        </div>
  
                        <!-- MASCOTA -->
                        <div class="form-group">
                            <label class="control-label col-md-3">Mascota</label>
                            <div class="col-md-9">
                                <select name="mascota_id" id="mascota_id" class="form-control" >
                                 
                                    <option selected value="">Seleccione mascota...</option> 
                                     <?php foreach($mascotas as $mascota){ ?>
                                        <option value="<?php echo $mascota->id?>" class="<?php echo $mascota->animal_id ?>"><?php echo $mascota->mascota_cliente ?> </option> 
                                    <?php } ?>    
                                </select>  
                   
                                <span class="help-block"></span>
                            </div>  
                         </div>                       
  
                         <!-- INGRESO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Ingreso</label>
                            <div class="col-md-9">
                             <input class="js-datepicker form-control" type="text" id="ingreso" name="ingreso" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                <span class="help-block"></span>
                            </div>
                        </div>

                         <!-- EGRESO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Egreso</label>
                            <div class="col-md-9">
                                 <input class="js-datepicker form-control" type="text" id="egreso" name="egreso" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- ESTADO 
                        <div class="form-group">
                            <label class="control-label col-md-3">Estado</label>
                            <div class="col-md-9">
                                   <select name="estado" id="estado" class="form-control" >
                                   <option selected value="" >Seleccione estado...</option> 
                                        <option value="0" >A Confirmar</option>  
                                        <option value="1" >Confirmada</option>  
                                    </select> 
                                <span class="help-block"></span>
                            </div>
                        </div>   
                        --> 

                        <!-- AREA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Area</label>
                            <div class="col-md-9">
                                   <select name="area_id" id="area_id" class="form-control" >
                                   <option selected value="-1" >Seleccione área...</option> 
                                    <?php foreach($areas as $area){ ?>
                                        <option value="<?php echo $area->id ?>" ><?php echo $area->nombre ?></option> 
                                    <?php } ?>    
                                    </select> 
                                <span class="help-block"></span>
                            </div>
                        </div> 

                        <!-- BOX -->
                        <div class="form-group">
                            <label class="control-label col-md-3">Box</label>
                            <div class="col-md-9">
                                <select name="box_id" id="box_id" class="form-control" >
                                    <option selected value="">Seleccione box...</option> 
                                     <?php foreach($boxes as $box){ ?>
                                        <option value="<?php echo $box->id?>" class="<?php echo $box->area_id ?>"><?php echo $box->nombre ?> </option> 
                                    <?php } ?>    
                                </select>  
                                <span class="help-block"></span>
                            </div>  
                         </div>

                        <!-- NOTAS --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Notas</label>
                            <div class="col-md-9">
                               <textarea class="form-control" name="notas" id="notas" rows="3"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    
