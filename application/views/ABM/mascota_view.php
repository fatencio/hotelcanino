<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7"> 
            <h1 class="page-heading">
                Mascotas
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('Mascota/index');">Mascotas</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_mascota()"><i class="glyphicon glyphicon-plus"></i> Nueva Mascota</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Cliente</th>
                        <th>Animal</th>
                        <th>Notas</th>
                        <th>Foto</th>                      
                        <th style="min-width:156px;">Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>                         
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>                
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 25,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Mascota/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false,
        },    
        { 
            "targets": [ 4 ], 
            "className": "hidden-xs hidden-sm hidden-md",       
        },              
        {
            "targets": [2,3],
            "className": "hidden-xs",       
        }  
        ],

       initComplete: function () {
            this.api().columns().every( function () 
            {

                var column = this;

                // Ddrop-down lists de busqueda
                if (column.index() == 2){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 110px; width: 200px; "><option value="">CLIENTE</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                            $('#table_ventas').DataTable().columns(3).search( val ? val : '', true, false ).draw();
                            $('#table_turnos').DataTable().columns(3).search( val ? val : '', true, false ).draw();
                        } );
     
                    <?php foreach($clientes as $cliente){ ?>
                        select.append( '<option value="<?php echo $cliente->nombre ?> <?php echo $cliente->apellido ?>"><?php echo $cliente->nombre ?> <?php echo $cliente->apellido ?></option>' )
                    <?php } ?>    
                }     

                // Animal
                if (column.index() == 3){
                    var select = $('<select class="form-control hidden-xs" style="position: absolute; top: -39px; left: 320px"><option value="">ANIMAL</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($animales as $animal){ ?>
                        select.append( '<option value="<?php echo $animal->nombre ?>"><?php echo $animal->nombre ?></option>' );
                    <?php } ?>    
                }                                                                        

            } );

        }, 

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });


    jQuery('.js-datepicker').add('.input-daterange').datepicker({
        weekStart: 1,
        autoclose: true,
        todayHighlight: true
    });

    ocultaSeccionesTipoAnimal();

});



function add_mascota()
{
    save_method = 'add';

    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'mascotas/miniaturas/mascota_avatar_mini.png' ?>');  
    $('#imagen').val('mascota_avatar_mini.png');

   
    $('#form')[0].reset(); // reset form on modals

//    $("#provincia").val(13);
//    $('[id="provincia"]').attr('disabled',true); //set provincia disable 


    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nueva Mascota'); // Set Title to Bootstrap modal title
}



function dias_transcurridos($fecha_i,$fecha_f)
{
    $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
    $dias   = abs($dias); $dias = floor($dias);     
    return $dias;
}






function edit_mascota(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
   // $('[id="provincia"]').attr('disabled',true); //set provincia disable 
    
    
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/Mascota/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            cambioTipoAnimal (data.animal_id);
            $('[name="animal_id"]').val(data.animal_id).change();
            
            

            $('[name="nacimiento"]').val(data.nacimiento);
            $('[name="fallecimiento"]').val(data.fallecimiento);
            
            
             var fechacero = '0000-00-00';
             var fechanula = '1970-01-01';

             var fallecimientoDate = $('[name="fallecimiento"]').val().replace('-','/');
             var ceroDate = fechacero.replace('-','/');
             var nulaDate = fechanula.replace('-','/');
            
             if(fallecimientoDate == ceroDate){
                $('[name="fallecimiento"]').val(" ");
             }
             else if (fallecimientoDate == nulaDate) {
                $('[name="fallecimiento"]').val(" ");
             }
             else {

                $('[name="fallecimiento"]').val(data.fallecimiento);   
             }
           
            $('[name="raza_id"]').val(data.raza_id);
            $('[name="sexo"]').val(data.sexo);
            $('[name="pelaje"]').val(data.pelaje);
            $('[name="tamanio_id"]').val(data.tamanio_id);
            $('[name="pesoingreso"]').val(data.pesoingreso);
            $('[name="pesosalida"]').val(data.pesosalida);
            $('[name="imagen"]').val(data.foto);  
            $('[name="cliente_id"]').val(data.cliente_id);
            $('[name="notas"]').val(data.notas);  

            


            $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'mascotas/miniaturas/' ?>'  + data.foto);  

          
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Mascota'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/Mascota/ajax_add";
         mensaje = 'Mascota creado.';

    } else {
        url = "<?php echo BASE_PATH ?>/Mascota/ajax_update";
         mensaje = 'Mascota modificado.';
    }


    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form'));

    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,           
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', mensaje); 
            }
            else
            {
                // Sesion expirada
                if (data.login) 
                {
                    window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                }
                else
                {                
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Mascota (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_mascota(nombre, id)
{
    if(confirm('¿Eliminar Mascota "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Mascota/ajax_delete_full/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {   
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'Mascota eliminada.');                 
                }
                else
                {
                    // Sesion expirada
                    if (data.login) 
                    {
                        window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                    }
                    else
                    {
                        aviso('danger', data.mensaje, "ERROR AL ELIMINAR Mascota");  
                        console.log(data.error);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                 aviso('danger', textStatus, "ERROR ELIMINANDO DATOS MASCOTA");  
                 console.log(textStatus);
            }
        });

    }
}


function switch_mascota(id){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Mascota/ajax_activar_desactivar/" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            if (data.status)
            {
                if (data.nuevo_status == 1)
                    aviso('success', 'Mascota habilitada.');  
                else
                    aviso('success', 'Mascota deshabilitada.');  
            }
            else
            {
                // Sesion expirada
                if (data.login) window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {   
            console.log("errorThrown: " + errorThrown);
            aviso('danger', textStatus, 'Error al activar / desactivar.'); 
        }
    });

}

function mostrarImagenes()
{
    $('#modal_imagenes').modal('show');
}


function elegirImagen(nombreImagen)
{
    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'mascotas/miniaturas/' ?>'  + nombreImagen);   
    $('#imagen').val(nombreImagen);

    $('#modal_imagenes').modal('hide');
}


function eliminarImagen(nombre)
{
    if(confirm('¿Eliminar imagen "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Mascota/delete_imagen/" + nombre,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                     $('[id="' + nombre + '"]').hide(); 

                     aviso('success', 'Imagen eliminada.');    
                }
                else
                {
                    alert(data.mensaje);
                    //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL ELIMINAR IMAGEN"); 
            }
        });
    }
}


  $(function(){

      $("#raza_id").chained("#animal_id");
      $("#tamanio_id").chained("#animal_id");
  });



function ocultaSeccionesTipoAnimal()
{
    $('[id="divRaza"]').hide(); 
    $('[id="divTamanio"]').hide(); 
  

}

function cambioTipoAnimal(id)
{
    // Inicialmente oculta todas las secciones que dependen del Animal seleccionado
    ocultaSeccionesTipoAnimal();

    // Muestra las secciones habilitadas para el Animal seleccionado
    if (typeof id !== "undefined")
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Animal/ajax_edit/" + id,        
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                // Raza
                conraza = (data.conraza == '1' ? true : false);
                if (conraza) $('[id="divRaza"]').show();

                // Tamaño
                contamanios = (data.contamanios == '1' ? true : false);
                if (contamanios) $('[id="divTamanio"]').show();

                                   
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                
            }
        });
    }
}






</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Mascota Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                         <!-- CLIENTE -->
                        <div class="form-group">
                            <label class="control-label col-md-3">Cliente</label>
                            <div class="col-md-4">
                                <select name="cliente_id" id="cliente_id" class="form-control" >
                                 
                                    <option selected value="">Seleccione cliente...</option> 
                                    <?php foreach($clientes as $cliente){ ?>
                                        <option value="<?php echo $cliente->id ?>" ><?php echo $cliente->apellido; echo " "; echo $cliente->nombre ?> </option> 
                                    <?php } ?>    
                                </select>  
                   
                                <span class="help-block"></span>
                            </div>  
                         </div>
                   
                        <!-- ANIMAL --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Animal</label>
                            <div class="col-md-9">
                                   <select name="animal_id" id="animal_id" class="form-control" onchange="cambioTipoAnimal(this.value)"  >                  >
                                   <option selected value="" >Seleccione tipo de animal...</option> 
                                    <?php foreach($animales as $animal){ ?>
                                        <option value="<?php echo $animal->id ?>" ><?php echo $animal->nombre ?></option> 
                                    <?php } ?>    
                                    </select> 
                                <span class="help-block"></span>
                            </div>
                        </div> 
  
                         <!-- NACIMIENTO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nacimiento</label>
                            <div class="col-md-9">
                             <input class="js-datepicker form-control" type="text" id="nacimiento" name="nacimiento" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                <span class="help-block"></span>
                            </div>
                        </div>

                         <!-- FALLECIMIENTO--> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Fallecimiento</label>
                            <div class="col-md-9">
                                 <input class="js-datepicker form-control" type="text" id="fallecimiento" name="fallecimiento" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- SEXO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Sexo</label>
                            <div class="col-md-9">
                            <select name="sexo" id="sexo" placeholder="Ingrese Sexo" class="form-control">
                                 <option selected value="Sin Sexo">Seleccione sexo...</option> 
                                 <option value="Macho">Macho</option> 
                                 <option value="Hembra">Hembra</option>
                            </select>
                            <span class="help-block"></span>
                            </div>
                            
                        </div>

                         <!-- RAZA --> 
                   
                        <div class="form-group" id="divRaza">
                            <label class="control-label col-md-3">Raza</label>
                            <div class="col-md-4" >
                                <select name="raza_id" id="raza_id" class="form-control" >
                                 
                                    <option selected value="">Seleccione raza...</option> 
                                  

                                    <?php foreach($razas as $raza){ ?>
                                        <option value="<?php echo $raza->id ?>" class="<?php echo $raza->animal_id ?>"><?php echo $raza->nombre ?></option> 
                                    <?php } ?>    



                                </select>  
                   
                                <span class="help-block"></span>
                            </div>  
                         </div>


                        <!-- PELAJE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Pelaje</label>
                            <div class="col-md-9">
                                <input name="pelaje" placeholder="Pelaje" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                          <!-- TAMAÑO --> 
                        
                          <div class="form-group" id="divTamanio">
                            <label class="control-label col-md-3">Tamaño</label>
                            <div class="col-md-4">
                                <select name="tamanio_id" id="tamanio_id" class="form-control" >
                                 
                                    <option selected value="">Seleccione tamaño...</option> 
                                    <?php foreach($tamanios as $tamanio){ ?>
                                        <option value="<?php echo $tamanio->id ?>" class="<?php echo $tamanio->animal_id ?>">><?php echo $tamanio->nombre ?> </option> 
                                    <?php } ?>    
                                </select>  
                   
                                <span class="help-block"></span>
                            </div>  
                         </div>

                         <!-- PESO INGRESO --> 
                         <div class="form-group">

                            <label class="control-label col-md-3">Peso Ingreso</label>
                            <div class="col-md-4">
                                <input name="pesoingreso" placeholder="Peso Ingreso" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                             <div class="col-md-5">
                                <label class="control-label"></label>
                            </div>
                          </div>
                          <div class="form-group">

                            <!-- PESO SALIDA --> 
                           
                            <label class="control-label col-md-3">Peso Salida</label>
                            <div class="col-md-4">
                                <input name="pesosalida" placeholder="Peso Salida" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                             <div class="col-md-5">
                                <label class="control-label"></label>
                            </div>
                          </div>

                           <!-- <div class="col-md-5">
                                <input name="localidad" placeholder="Localidad" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                            
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-4">
                                <select name="departamento" id="departamento" class="form-control" >
                                 
                                    <option selected value="">Seleccione departamento...</option> 
                                    <?php foreach($departamentos as $departamento){ ?>
                                        <option value="<?php echo $departamento->id ?>" ><?php echo $departamento->nombre ?></option> 
                                    <?php } ?>    
                                </select>  
                   
                                <span class="help-block"></span>
                            </div>  


                            <div class="col-md-5">
                                <select name="provincia" id="provincia" class="form-control" >
                                 
                                    <option selected value="">Seleccione provincia...</option> 
                                    <?php foreach($provincias as $provincia){ ?>
                                        <option value="<?php echo $provincia->id ?>" ><?php echo $provincia->nombre ?></option> 
                                    <?php } ?>    
                                </select>  
                   
                                <span class="help-block"></span>
                            </div>-->                                                      
                      

              


                     

                         <!-- IMAGEN --> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Imagen</label>
                            <div class="col-md-10">
                                <button type="button" id="btnElegirImagen" onclick="mostrarImagenes()" class="btn btn-default pull-right">o elegir una imagen del sitio</button>    

                                <input name="foto" type="file" id="foto" size="40" class="btn btn-default pull-left" >  
                                <div class="clearfix"></div>  
                                <input type="hidden" value="MASCOTA_avatar_mini.png" id="imagen" name="imagen" />
                                <span class="help-block"></span>
                                
                                <div class="clearfix"></div>    
                                <br/>                                                       
                                <img class="centrado" src="<?php echo IMG_PATH . 'mascotas/miniaturas/mascota_avatar_mini.png' ?>" id="imagenPreview" /> 
                            </div>
                        </div>


                        <!-- NOTAS --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Notas</label>
                            <div class="col-md-9">
                               <textarea class="form-control" name="notas" id="notas" rows="3"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_password" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Modificar Contraseña</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_password" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="mascota_id"/> 
                    <div class="form-body">

                        <!-- NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nueva password</label>
                            <div class="col-md-9">
                                <input name="password" placeholder="Nueva password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- REPITE NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Repita password</label>
                            <div class="col-md-9">
                                <input name="password2" placeholder="Repita password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_Password" onclick="save_password()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal --> 

<!-- Bootstrap modal de imagenes -->
<div class="modal fade" id="modal_imagenes" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Imágenes del sitio</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/mascotas/miniaturas/*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn"  id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegirImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                            <br/><br/><br/>
                                            <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminarImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>                                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->   