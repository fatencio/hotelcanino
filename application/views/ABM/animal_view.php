<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Animales
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('Animal/index');">Animales</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_animal()"><i class="glyphicon glyphicon-plus"></i> Nuevo Animal</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content"> 
            <table id="table" class="table table-bordered table-striped js-dataTable-full js-table-checkable"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Con Raza</th>
                        <th>Con Tamaños</th>
                        <th style="width:20px;">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; 
var table;

$(document).ready(function() {

    table = $('#table').DataTable({ 

        "processing": true, 
        "serverSide": true, 
        "order": [], 

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Animal/ajax_list",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1], 
            "orderable": false, 
        },
        ],

    });


    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_animal()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Nuevo Animal'); 
}

function edit_animal(id)
{
    save_method = 'update';
    $('#form')[0].reset(); 
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Animal/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);

            contamanios = (data.contamanios == '1' ? true : false);
            $('[name="contamanios"]').prop('checked', contamanios);

            conraza = (data.conraza == '1' ? true : false);     
            $('[name="conraza"]').prop('checked', conraza);       

            $('#modal_form').modal('show'); 
            $('.modal-title').text('Editar Animal');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); 
}

function save()
{
    $('#btnSave').text('Guardando...'); 
    $('#btnSave').attr('disabled',true); 
    var url;
    var mensaje;


    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/Animal/ajax_add";
        mensaje = 'Animal creado.';

    } else {
        url = "<?php echo BASE_PATH ?>/Animal/ajax_update";
        mensaje = 'Animal modificado.'
    }


    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();

                aviso('success', mensaje);  
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            
            aviso('danger', textStatus, 'Error al crear o modificar Animal (' + errorThrown + ')'); 

            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_animal(nombre, id)
{
    if(confirm('¿Eliminar Animal "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Animal/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'Animal eliminado.');                 
                }
                else
                {
                    aviso('danger', data.mensaje, "ERROR AL ELIMINAR ANIMAL");  
                    console.log(data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
              
                console.log(textStatus);
            }
        });

    }
}


</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Animal Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- CON RAZA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Con Raza</label>
                            <div class="col-md-9">
                                <label class="css-input switch switch-info">
                                    <input type="checkbox" name="conraza"><span></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- CON TAMANIOS --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Con Tama&ntilde;os</label>
                            <div class="col-md-9">
                                <label class="css-input switch switch-info">
                                    <input type="checkbox" name="contamanios"><span></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    