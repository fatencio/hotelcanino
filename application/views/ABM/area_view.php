<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Areas
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('Area/index');">Areas</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_area()"><i class="glyphicon glyphicon-plus"></i> Nueva Area</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <!-- AREAS -->
        <div class="block-content"> 
            <table id="table_area" class="table table-bordered table-striped js-dataTable-full js-table-checkable"  cellspacing="0" width="100%">
                <thead>
                    <tr >
                        <th></th>
                        <th></th>
                        <th style="width:150px;">Area</th>
                        <th>Notas</th>
                        <th style="width:160px;">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>            
    </div>         
</div>

<script type="text/javascript">

var save_method; 
var table_area;
var table_box;


$(document).ready(function() 
{
    table_area = $('#table_area').DataTable({ 

        "processing": true, 
        "serverSide": true, 
        "ordering": false,
        "filter": false,
        "paging": false,

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Area/ajax_list",
            "type": "POST"
        },

        "columnDefs": [
        { 
            'targets': [ 0, 1 ],
            "visible": false,
        },
        { 
            'targets': [ 2 ],
            "className": 'details-control'
        },    
        {
            "targets": [3],
            "className": "hidden-xs",       
        }              
        ],
    });


    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});


/* BOXES DE CADA AREA */

// Array to track the ids of the details displayed rows
var detailRows = [];
var detailRowsAux = [];

$('#table_area tbody').on( 'click', 'tr td.details-control', function () 
{
    var tr = $(this).closest('tr');
    var row = table_area.row( tr );
    var idx = $.inArray( tr.attr('id'), detailRows );

    if ( row.child.isShown() ) {
        tr.removeClass( 'details' );
        row.child.hide();

        // Remove from the 'open' array
        detailRows.splice( idx, 1 );
    }
    else {
        tr.addClass( 'details' );
        row.child( mostrar_boxes( row.data() ) ).show();

        // Add to the 'open' array
        if ( idx === -1 ) {
            detailRows.push( tr.attr('id') );
        }
    }

   // detailRows = [];
} );

// On each draw, loop over the `detailRows` array and show any child rows
table_area.on( 'draw', function () {
    console.log('draw');
    $.each( detailRows, function ( i, id ) 
    {
        console.log(id);
        $('td.details-control').trigger( 'click' );
    } );
} );


function mostrar_boxes(d) 
{
    return d[1];
}

/* fin BOXES DE CADA AREA */


function add_area()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Nueva Area'); 
}

function edit_area(id)
{
    save_method = 'update';
    $('#form')[0].reset(); 
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Area/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="notas"]').val(data.notas);

            $('#modal_form').modal('show'); 
            $('.modal-title').text('Editar Area');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    //detailRowsAux = detailRows;
    table_area.ajax.reload(null,false); 

    //detailRows = detailRowsAux;
}

function save()
{
    $('#btnSave').text('Guardando...'); 
    $('#btnSave').attr('disabled',true); 
    var url;
    var mensaje;


    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/Area/ajax_add";
        mensaje = 'Area creada.';

    } else {
        url = "<?php echo BASE_PATH ?>/Area/ajax_update";
        mensaje = 'Area modificada.'
    }


    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) 
            {
                $('#modal_form').modal('hide');
                reload_table();

                aviso('success', mensaje);  
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            
            aviso('danger', textStatus, 'Error al crear o modificar Area (' + errorThrown + ')'); 

            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_area(nombre, id)
{
    if(confirm('¿Eliminar Area "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Area/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'Area eliminada.');                 
                }
                else
                {
                    aviso('danger', data.mensaje, "ERROR AL ELIMINAR Area");  
                    console.log(data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
              
                console.log(textStatus);
            }
        });

    }
}



/* BOXES */

function add_box(area_id, area_nombre)
{
    save_method = 'add';
    $('#form_box')[0].reset(); // reset form on modals
    $('.form-group-box').removeClass('has-error'); // clear error class
    $('.help-block-box').empty(); // clear error string

    $('[name="area_id"]').val(area_id);

    $('#modal_form_box').modal('show'); // show bootstrap modal
    $('.modal-title-box').text('Area ' + area_nombre + ' - Nuevo Box'); // Set Title to Bootstrap modal title
}

function edit_box(id)
{
    save_method = 'update';
    $('#form_box')[0].reset(); // reset form on modals
    $('.form-group-box').removeClass('has-error'); // clear error class
    $('.help-block-box').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/Box/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_box"]').val(data.id);
            $('[name="nombre_box"]').val(data.nombre);
            $('[name="capacidad_box"]').val(data.capacidad);
            $('[name="notas_box"]').val(data.notas);
            $('[name="area_id"]').val(data.box_area_id);

            $('#modal_form_box').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title-box').text('Editar Box'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
              aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}


function save_box()
{
    $('#btnSave_box').text('Guardando...'); //change button text
    $('#btnSave_box').attr('disabled',true); //set button disable 
    var url;
    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/Box/ajax_add";
         mensaje = 'Box creado.';
    } else {
        url = "<?php echo BASE_PATH ?>/Box/ajax_update";
         mensaje = 'Box modificado.';
    }


    // ajax adding data to database
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_box').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form_box').modal('hide');
                reload_table();
                aviso('success', mensaje);  
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave_box').text('Guardar'); //change button text
            $('#btnSave_box').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Box (' + errorThrown + ')'); 
           
            $('#btnSave_box').text('Guardar'); //change button text
            $('#btnSave_box').attr('disabled',false); //set button enable 

        }
    });
}


function delete_box(nombre, id)
{
    if(confirm('¿Eliminar Box "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Box/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                if(data.status) //if success close modal and reload ajax table
                {


                $('#modal_form_box').modal('hide');
                reload_table();
                 aviso('success', 'Box eliminado.');      
                }
                else {
                 aviso('danger', data.mensaje, "ERROR AL ELIMINAR BOX");  
               
                 console.log(data.error);

                 }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                   aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
            }
        });

    }
}
/* fin BOXES */


</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Area Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                      
                        <!-- NOTAS --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Notas</label>
                            <div class="col-md-9">
                               <textarea class="form-control" name="notas" id="notas" rows="3"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                     

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    


<!-- Bootstrap modal BOX -->
<div class="modal fade" id="modal_form_box" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-box">Box Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_box" class="form-horizontal">
                    <input type="hidden" value="" name="id_box"/> 
                    <input type="hidden" value="" name="area_id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre_box" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <!-- CAPACIDAD --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="capacidad_box" placeholder="Capacidad" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                        </div>        

                        <!-- NOTAS --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Notas</label>
                            <div class="col-md-9">
                               <textarea class="form-control" name="notas_box" id="notas_box" rows="3"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_box" onclick="save_box()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal BOX-->