<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/*
$route['register'] = 'user/register';
$route['login'] = 'user/login';
$route['logout'] = 'user/logout';
*/

$route['default_controller'] = 'inicio';

/*
$route['(:any)'] = "$1";
$route['beneficios'] = 'inicio/beneficios';
$route['terminos'] = 'inicio/terminos';
$route['politicas'] = 'inicio/politicas';
$route['faq'] = 'inicio/faq';
$route['mapa'] = 'inicio/mapa';
$route['contacto'] = 'inicio/contacto';
$route['contacto_local'] = 'inicio/contacto_local';
$route['solicitar_restablecer'] = 'inicio/solicitar_restablecer';
$route['restablecer'] = 'inicio/restablecer';
$route['activar'] = 'inicio/activar';
$route['dashboard/(:any)'] = 'inicio/dashboard/$1';
*/
//$route['subjects/(:num)/(:any)'] = 'subjects/view/$1/$2';

//$route['subjects/(:any)'] = 'subject/view

/*
$config['admin'] = "inicio";  #http://example.com/admin

//$route['default_controller'] = "inicio/index";
$route['admin'] = "inicio";
$route['404_override'] = '';
*/
/*
$route['some_name'] = 'frontend/some_name'; #http://example.com/some_name
$route['default_controller'] = 'main'; #http://example.com/
*/