<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Tools 
{
	// Devuelve los elementos no comunes a dos arrays
	public function array_diferencia($A, $B) 
	{
	    $intersect = array_intersect($A, $B);
	    return array_merge(array_diff($A, $intersect), array_diff($B, $intersect));
	}

	// Valida que un valor ingresado sea un número natural
	function is_natural($natural, $zero=true)
	{
		if(ctype_digit($natural))
		{
		    if($zero)
		    {
		        $natural_test=(int)$natural;
		        if((string)$natural_test !== $natural) return false;
		        else return true;
		    }
		    else return true; 
		}
		else return false;    
	}


	public function hace_fecha($fecha)
	{
	    $hora = $fecha->format('H:i');
	    $dia = $fecha->format('d');
	    $mes = $fecha->format('m');

		$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre");

		$dias =  $dia . " de " . $meses[intval($mes)-1];

		$fecha = $fecha->getTimestamp();

	    $ahora = time();

	    $tiempo = $ahora - $fecha;

	     if(round($tiempo / 31536000) <= 0){ 
	        if(round($tiempo / 2678400) <= 0){ 
	             if(round($tiempo / 86400) <= 0){ 
	                if(round($tiempo / 3600) <= 0){ 
	                    if(round($tiempo / 60) <= 0){ 
	                if($tiempo <= 60){ $hace = "Hace instantes"; } 

	                } else  
	                { 
	                    $can = round($tiempo / 60);
	                    $can2 = date("s",$tiempo);    //segundos        
	                    if($can <= 1) {    $word = "minuto"; } else { $word = "minutos"; } 
	                    
		                if ($can2 != 0)	                    
	                    	$hace = "Hace " .$can. " ".$word." y " .$can2. " segundos";
	                    else
	                    	$hace = "Hace " .$can. " ".$word;
	                }
	                
	                } else  
	                { 
	                    $can = round($tiempo / 3600);
	                    $can2 = round(date("i",$tiempo)); //minutos
	                    if($can <= 1) {    $word = "hora"; } else {    $word = "horas"; } 

	                    if ($can2 != 0)
	                    	$hace = "Hace " .$can. " ".$word." y " .$can2. " minutos"; 
	                    else
	                    	$hace = "Hace " .$can. " ".$word; 

	                } 
	                } else  
	                { 
	                    $can = round($tiempo / 86400);
	                    $can2 = round(date("H",$tiempo)); //horas
	                    if($can <= 1) {    $word = "día"; } else {    $word = "días"; } 

	                    if ($word == "día")
	                    	$hace = "Ayer a las " . $hora;
	                    else
	                    	$hace = $dias . " a las " . $hora;
	                } 
	                } else  
	                { 
	                	$hace = $dias . " a las " . $hora;
	                } 
	                } else  
	                { 
	                	$hace = $dias . " a las " . $hora;
	                }

	    return $hace; 
	}

	public function hace_fecha_tiempo_transcurrido($fecha){
	   // $fecha = $fecha; 
	    $ahora = time();
	    $tiempo = $ahora-$fecha;
	     
	     if(round($tiempo / 31536000) <= 0){ 
	        if(round($tiempo / 2678400) <= 0){ 
	             if(round($tiempo / 86400) <= 0){ 
	                if(round($tiempo / 3600) <= 0){ 
	                    if(round($tiempo / 60) <= 0){ 
	                if($tiempo <= 60){ $hace = "Hace instantes"; } 

	                } else  
	                { 
	                    $can = round($tiempo / 60);
	                    $can2 = date("s",$tiempo);    //segundos        
	                    if($can <= 1) {    $word = "minuto"; } else { $word = "minutos"; } 
	                    $hace = "Hace " .$can. " ".$word." y " .$can2. " segundos";
	                }
	                
	                } else  
	                { 
	                    $can = round($tiempo / 3600);
	                    $can2 = round(date("i",$tiempo)); //minutos
	                    if($can <= 1) {    $word = "hora"; } else {    $word = "horas"; } 
	                    $hace = "Hace " .$can. " ".$word." y " .$can2. " minutos"; 
	                } 
	                } else  
	                { 
	                    $can = round($tiempo / 86400);
	                    $can2 = round(date("H",$tiempo)); //horas
	                    if($can <= 1) {    $word = "día"; } else {    $word = "días"; } 

	                    $hace = "Hace " .$can. " ".$word." y " .$can2. " horas";
	                } 
	                } else  
	                { 
	                    $can = round($tiempo / 2678400);  
	                    $can2 = round(date("j",$tiempo)); //dias
	                    if($can <= 1) {    $word = "mes"; } else { $word = "meses"; } 
	                    $hace = "Hace " .$can. " ".$word." y " .$can2. " días";
	                } 
	                } else  
	                { 
	                    $can = round($tiempo / 31536000); 
	                    if($can <= 1) {    $word = "año";} else { $word = "años"; } 
	                    $hace = "Hace " .$can. " ".$word."";
	                    
	                }

		    return $hace; 
		}		


	// Calcula la distancia entre dos puntos geograficos
	public function distancia($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
	    return $miles * 1.609344;
	  } else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
	        return $miles;
	      }
	}	


	// Calcula la distancia en metros o km 
	public function format_distancia($kilometros){

		if ($kilometros < 1)
			return ceil($kilometros * 1000) . ' mts.';
		else
			return round($kilometros, 2) . ' kms.';

	}	


	public function mesSiguiente($fecha)
	{
		$fecha = explode('-',$fecha);
		if($fecha[1]==12)
			$sig='01-01-'.($fecha[2]+1);
		else
			{
			$nuevomes=$fecha[1]+1;
			if($nuevomes<=9)
				$nuevomes="0".$nuevomes;
			$sig='01-'.($nuevomes).'-'.$fecha[2];	
		}
		return $sig;	
	}


	public function mesAnterior($fecha)
	{
		$fecha = explode('-',$fecha);
		if($fecha[1]==1)
			$sig='01-12-'.($fecha[2]-1);
		else
		{
			$nuevomes=$fecha[1]-1;
			if($nuevomes<=9)
				$nuevomes="0".$nuevomes;
			$sig='01-'.($nuevomes).'-'.$fecha[2];	
		}
			
		return $sig;	
	}


	public function idddeMes($fecha)
	{
		$fecha = explode('-',$fecha);	
		return $fecha[1];
	}


	public function idDeAnio($fecha)
	{
		$fecha = explode('-',$fecha);	
		return $fecha[2];
	}


	public function mesPrimerDia($fecha)
	{
		$fecha = explode('-',$fecha);	
		return $fecha[2].$fecha[1].'01';	
	}


	public function mesUltimoDia($f)
	{
		$fecha = explode('-',$f);	
		return $fecha[2].$fecha[1].$this->cantidadDiasdelMes($f);	
	}


	public function cantidadDiasdelMes($fecha)
	{
		$timestamp = strtotime( $fecha );
		$diasdelmes = date( "t", $timestamp );
		return $diasdelmes;
	}	


	public function diaSemana($d) //feed de fecha en espanio dd-mm-aaaa
	{
		$spDate = explode ('-', $d); //mes - dia - anio
		$unix = mktime (0,0,0, $spDate[1], $spDate[0], $spDate[2]);	
		$fecha = date ('w',$unix);	
		$dia_sem = array(7,1,2,3,4,5,6);
		return $dia_sem[$fecha];
	}


	public function diaSemanaOF($d) //feed de fecha en aaaammdd
	{
		$d = str_split($d);
		$dia = $d[6].$d[7];
		$mes = $d[4].$d[5];
		$anio=$d[0].$d[1].$d[2].$d[3];
		$unix = mktime (0,0,0, $mes, $dia, $anio);	
		$fecha = date ('w',$unix);	
		$dia_sem = array(7,1,2,3,4,5,6);
		return $dia_sem[$fecha];
		
	}	


    public function _data_first_month_day($fecha) { //en espanio dd-mm-aaaa
        $f = explode('-', $fecha);
	    $month = $f[1];
        $year = $f[2];
        return date('d-m-Y', mktime(0,0,0, $month, 1, $year));
    }	


	public function nombreMes($mes)
	{
		$spDate = explode('-',$mes);

		$nombre_mes = array("Enero","Febrero","Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		$nroMes = $spDate['1']-1;
		return $nombre_mes[$nroMes];
		
	}	

	// Calcula la cantidad de dias entre dos fechas (soporta valores negativos)
	function cuentaDias($start_date, $end_date){

	 //  $d1 = new DateTime($start_date);
	 //  $d2 = new DateTime($end_date);
	   $difference = $start_date->diff($end_date);

	   if ($difference->invert == 1) return $difference->d;
	   else return -$difference->d;
	}	
}