<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Name of Class as mentioned in $hook['post_controller]
class Db_log {
 
    function __construct() {
       // Anything except exit() :P
    }
 
    // Name of function same as mentioned in Hooks Config
    function logQueries() {
 
        $CI = & get_instance();
 
        // hora de ejecucion
        $dt = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));
        $hora = $dt->format('d/m/Y, H:i:s');

        $filepath = APPPATH . 'logs/Query-log-' . date('Y-m-d') . '.php'; // Creating Query Log file with today's date in application/logs folder
        $handle = fopen($filepath, "a+");                 // Opening file with pointer at the end of the file
 
        $times = $CI->db->query_times;                   // Get execution time of all the queries executed by controller

        foreach ($CI->db->queries as $key => $query) 
        { 
            // ruta de la funcion invocada
            $ruta = $CI->router->directory . " > " . $CI->router->class . " > " . $CI->router->method . "\n\n";

            // query + execution time
            $sql = $query . " \n\nExecution Time:" . $times[$key];

            // escribe salida
            fwrite($handle, $hora . $ruta . $sql . "\n\n\n\n");    
        }
 
        fclose($handle);      // Close the file
    }
 
}