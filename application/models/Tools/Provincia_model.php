<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provincia_model extends CI_Model {

	var $table = 'provincia';

	public function get_all()
	{       
		$this->db->select('provincia_id as id, provincia_nombre as nombre');
		$this->db->from($this->table);
		$query = $this->db->get();

		return $query->result();
	}	

}