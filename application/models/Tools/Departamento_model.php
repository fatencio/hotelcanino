<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento_model extends CI_Model {

	var $table = 'departamento';

	public function get_all()
	{       
		$this->db->select('departamento_id as id, departamento_nombre as nombre');
		$this->db->from($this->table);
		$query = $this->db->get();

		return $query->result();
	}	

}