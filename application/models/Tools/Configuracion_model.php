<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion_model extends CI_Model {

	var $table = 'configuracion';

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function get_value($item)
	{       
    	$this->db->select('"CONFIGURACION - get_value", configuracion_valor as valor', false)
        	->from($this->table)
			->where('configuracion_item',$item);

		$query = $this->db->get();

		return $query->row('valor');
	}	


	public function get_value2($item)
	{       
    	$this->db->select('"CONFIGURACION - get_value", configuracion_valor2 as valor', false)
        	->from($this->table)
			->where('configuracion_item',$item);

		$query = $this->db->get();

		return $query->row('valor');
	}	



	// Valida si la imagen $name está siendo utilizada en alguna portada
	public function check_imagen_portada($name)
	{
		$this->db->from($this->table);
		$this->db->where('configuracion_valor', $name);

		return $this->db->count_all_results();
	}		
}