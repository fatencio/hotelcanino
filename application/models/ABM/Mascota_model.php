<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mascota_model extends CI_Model {

	var $table = 'mascota';

	var $column_order = array('mascota_nombre','cliente_nombre', 'animal_nombre', 'mascota_notas', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('mascota_nombre','cliente_nombre','cliente_apellido', 'animal_nombre', 'mascota_notas');  // columnas con la opcion de busqueda habilitada

	var $order = array('mascota_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{
        $this->db->select('mascota_id as id, mascota_nombre as nombre, animal_nombre as animal, CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente, mascota_notas as notas, mascota_foto as avatar, mascota_status as status', false);
		
		$this->db->join('animal', 'animal_id = mascota_animal_id');  
		$this->db->join('cliente', 'cliente_id = mascota_cliente_id'); 
		
        $this->db->from($this->table);
        $this->db->where('cliente_usuario_baja', '');


		$i = 0;
	
		// Busqueda por drop-down de clientes
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('CONCAT(cliente_nombre, " " , cliente_apellido)', $_POST['columns'][2]['search']['value'], false);
		}

		// Busqueda por drop-down de tipo de animal
		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('animal_nombre', $_POST['columns'][3]['search']['value']);
		}

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);		
		$this->db->join('animal', 'animal_id = mascota_animal_id');  
		$this->db->join('cliente', 'cliente_id = mascota_cliente_id'); 


		return $this->db->count_all_results();
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);
        return $this->db->insert_id();
	}


	public function get_by_id($id)
	{       
        $this->db-> select('mascota_id as id, 
        					CONCAT("M", LPAD(mascota_id, 3, "0")) AS codigo, 
        					mascota_nombre as nombre,
        					mascota_animal_id as animal_id,
        					animal_nombre as tipo,
        					DATE_FORMAT(mascota_nacimiento, "%d/%m/%Y") AS nacimiento,
        					DATE_FORMAT(mascota_fallecimiento, "%d/%m/%Y") AS fallecimiento,
        					mascota_raza_id as raza_id,
        					mascota_sexo as sexo,
        					mascota_pelaje as pelaje,
        					mascota_tamanio_id as tamanio_id,
        					mascota_pesoingreso as pesoingreso,
        					mascota_pesosalida as pesosalida,
        					mascota_foto as foto,
        					mascota_notas as notas,
        					mascota_status as status,
        					cliente_id,
        					CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente, 
        					tamanio_id as tamanio, 
                            raza_id as raza', false);

        $this->db->from($this->table);
        $this->db->join('animal', 'animal_id = mascota_animal_id');   
        $this->db->join('cliente', 'cliente_id = mascota_cliente_id');  
        $this->db->join('tamanio', 'tamanio_id = mascota_tamanio_id', 'left');  
        $this->db->join('raza', 'raza_id = mascota_raza_id', 'left');     
        $this->db->where('mascota_id', $id);

		$query = $this->db->get();

		return $query->row();
	}



	public function update($where, $data)
	{    			
		$this->db->update($this->table, $data, $where);

		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('mascota_id', $id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}
		

		return $retorno;		
	}
	

	// Valida si la foto $name está siendo utilizada en alguna mascota
	public function check_foto($name)
	{
		$this->db->from($this->table);
		$this->db->where('mascota_foto', $name);

		return $this->db->count_all_results();
	}	


	public function get_all($solo_habilitadas = FALSE)
	{       
		$this->db->select('mascota_id as id, mascota_nombre as nombre, mascota_animal_id as animal_id, CONCAT(UPPER(mascota_nombre), " (", cliente_nombre, " " , cliente_apellido, ")") AS mascota_cliente');

		$this->db->from($this->table);
		$this->db->join('cliente', 'cliente_id = mascota_cliente_id'); 

		if ($solo_habilitadas) $this->db->where('mascota_status', 1);

		$this->db->order_by('mascota_nombre');

		$query = $this->db->get();

		return $query->result();
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('mascota_nombre', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('mascota_nombre', $name);
		$this->db->where('mascota_id !=', $id);

		return $this->db->count_all_results();
	}	
}