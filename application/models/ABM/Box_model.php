<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Box_model extends CI_Model {

	var $table = 'box';

	var $column_order = array('box_nombre', 'box_area_id', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('box_nombre', 'area_nombre'); 		// columnas con la opcion de busqueda habilitada

	var $order = array('box_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query($area_id = '')
	{     
		$this->db->select('box_id as id, box_nombre as nombre, box_area_id as area_asignada, area_nombre as box_area_asignada_nombre, box_capacidad as capacidad, box_notas as notas');
        $this->db->from($this->table);
		$this->db->join('area', 'box_area_id = area_id');

		if ($area_id != '') $this->db->where('box_area_id',$area_id);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables($area_id = '')
	{
		$this->_get_datatables_query($area_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
		$this->db->select('box_id as id, box_nombre as nombre, box_capacidad as capacidad, box_notas as notas, box_area_id');
		$this->db->from($this->table);
		$this->db->where('box_id',$id);
		$query = $this->db->get();

		return $query->row();
	}


	public function check_duplicated($name, $area)
	{
		$this->db->from($this->table);
		$this->db->where('box_nombre', $name);
		$this->db->where('box_area_id', $area);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name, $area)
	{
		$this->db->from($this->table);
		$this->db->where('box_nombre', $name);
		$this->db->where('box_area_id', $area);
		$this->db->where('box_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{   

		$retorno = "";
		$this->db->where('box_id', $id);
		 if (!$this->db->delete($this->table)) {
            $retorno = $this->db->error();

		 }

		 return $retorno;
	}
	

	public function get_all()
	{   
	    $this->db->select('box_id as id, box_nombre as nombre, box_area_id as area_id');    
		$this->db->from($this->table);
		$this->db->order_by('box_nombre');

		$query = $this->db->get();

		return $query->result();
	}		


	public function get_all_with_area()
	{       
		$this->db->select('box_id as id, box_nombre as nombre, box_area_id as area_id, area_nombre');
		$this->db->from($this->table);		
		$this->db->join('area', 'box_area_id = area_id');		
		$this->db->where('box_capacidad >', 'box_lugares_ocupados', FALSE);		
		$this->db->order_by('box_nombre');

		$query = $this->db->get();

		return $query->result();
	}
	

	public function get_by_area($area_id)
	{       
		$this->db->select('box_id as id, box_nombre as nombre, box_area_id as area_id');
		$this->db->from($this->table);			
		$this->db->where('box_area_id', $area_id);		
		$this->db->where('box_status', 1);		
		$this->db->order_by('box_nombre');

		$query = $this->db->get();

		return $query->result();
	}
}