<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class reserva_model extends CI_Model {

	var $table = 'reserva';

/*
	var $column_order = array('reserva_nombre','cliente_nombre', 'reserva_notas', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('reserva_nombre','cliente_nombre', 'reserva_notas');  // columnas con la opcion de busqueda habilitada

	var $order = array('reserva_nombre' => 'asc'); // default order 
*/

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all()
	{       
		$this->db->select('reserva_id as id, 
						   COALESCE(reserva_box_id, -1) as box_id, 
						   mascota_nombre as mascota, 
						   CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente, 
						   reserva_fecha_desde as fecha_desde, 
						   reserva_fecha_hasta as fecha_hasta', false);
		$this->db->from($this->table);
		$this->db->join('mascota', 'mascota_id = reserva_mascota_id');
		$this->db->join('cliente', 'cliente_id = mascota_cliente_id'); 
		$query = $this->db->get();

		return $query->result();
	}

	
	public function save($data)
	{
		$this->db->insert($this->table, $data);
        return $this->db->insert_id();
	}


	public function get_by_id($id)
	{       
		$this->db->select('reserva_id as id, 
						   COALESCE(box_area_id, -1) as area_id, 
						   COALESCE(reserva_box_id, -1) as box_id, 
						   mascota_animal_id as animal_id, 
						   mascota_id, 
						   mascota_nombre as mascota, 
						   CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente, 
						   DATE_FORMAT(reserva_fecha_desde, "%d/%m/%Y") AS fecha_desde, 
						   DATE_FORMAT(reserva_fecha_hasta, "%d/%m/%Y") AS fecha_hasta,
						   reserva_notas as notas', false);
		$this->db->from($this->table);
		$this->db->join('box', 'box_id = reserva_box_id', 'left');
		$this->db->join('mascota', 'mascota_id = reserva_mascota_id');
		$this->db->join('cliente', 'cliente_id = mascota_cliente_id'); 
        $this->db->where('reserva_id', $id);

		$query = $this->db->get();

		return $query->row();
	}


	public function update($where, $data)
	{    			
		$this->db->update($this->table, $data, $where);

		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('reserva_id', $id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}
		

		return $retorno;		
	}
	
/*
	private function _get_datatables_query()
	{
        $this->db->select('reserva_id as id, mascota_nombre as mascota, CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente, reserva_notas as notas, reserva_fecha_desde, reserva_estado as estado', false);
		
		$this->db->join('mascota', 'mascota_id = reserva_mascota_id');  
		$this->db->join('cliente', 'cliente_id = reserva_cliente_id'); 
		$this->db->join('box', 'box_id = reserva_box_id'); 
		
        $this->db->from($this->table);
        $this->db->where('cliente_usuario_baja', '');


		$i = 0;
	
		// Busqueda por drop-down de clientes
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('CONCAT(cliente_nombre, " " , cliente_apellido)', $_POST['columns'][2]['search']['value'], false);
		}

		// Busqueda por drop-down de tipo de animal
		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('mascota _nombre', $_POST['columns'][3]['search']['value']);
		}

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);		
		$this->db->join('animal', 'animal_id = reserva_animal_id');  
		$this->db->join('cliente', 'cliente_id = reserva_cliente_id'); 


		return $this->db->count_all_results();
	}
*/



}