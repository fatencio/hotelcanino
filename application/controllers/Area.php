<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/area_model','area');
		$this->load->model('ABM/box_model','box');
		$this->load->library('validations');

		$this->load->helper('url');
	}


	public function index()
	{
		$this->load->view('ABM/area_view');
	}


	public function ajax_list()
	{
		$list = $this->area->get_datatables();
		$data = array();
		$boxes = array();
		$boxes_html = '';
		$no = $_POST['start'];

		foreach ($list as $area) 
		{
			$boxes_html = '';
			$no++;

			// Carga boxes de cada area
			$boxes = $this->box->get_datatables($area->id);


			$boxes_html = '<button class="btn btn-xs btn-success" style="margin-left: 3px;" onclick="add_box('. $area->id .', '."'".$area->nombre."'".');"><i class="glyphicon glyphicon-plus"></i> Nuevo Box</button>
				<div style="height: 16px"></div>';

			if (count($boxes) > 0)
			{
				$boxes_html .= '
				<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%" role="grid" aria-describedby="table_info" style="width: 100%;">
                <thead>
                    <tr role="row">
                    	<th tabindex="0" >Box</th>
                    	<th tabindex="0" >Capacidad</th>
                    	<th tabindex="0" class="hidden-xs">Notas</th>
                    	<th style="min-width: 116px; max-width: 116px; width: 116px;">Acción</th></tr>
                </thead>
                <tbody>';

				foreach ($boxes as $box) 
				{				
	                $boxes_html .= '
	                <tr role="row" class="odd">
	                	<td>' . $box->nombre . '</td>
	                	<td>' . $box->capacidad . '</td>
	                	<td class="hidden-xs">' . $box->notas . '</td>
	                	<td>
	                		<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar Box" onclick="edit_box(' . $box->id . ')"><i class="glyphicon glyphicon-pencil"></i></a>
					  		<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar Box" onclick="delete_box(\'' . $box->nombre . '\',' . $box->id . ')"><i class="glyphicon glyphicon-trash"></i></a>
							<label class="css-input switch switch-sm switch-info" title="Activar / Desactivar Box">
			 					<input type="checkbox" name="switch_cliente" checked="" onclick="switch_box(' . $box->id . ')"><span></span>
             				</label>					  		
					  	</td>
					 </tr>';
				}                

				$boxes_html .= '</tbody></table>';
            	
			}
			else
			{
				$boxes_html .= 'El area seleccionada no tiene boxes.';
			}

			$row = array();

			$row[] = $area->id;


			$row[] = $boxes_html;			
			$row[] = '<a class="btn btn-sm btn-success" title="Ver Boxes" style="width:200px;">' . $area->nombre . ' (' . count($boxes) . ' boxes)</a>';
			$row[] = $area->notas;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Editar Area" onclick="edit_area('."'".$area->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Eliminar Area" onclick="delete_area('."'".$area->nombre."'".',' .$area->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->area->count_all(),
						"recordsFiltered" => $this->area->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->area->get_by_id($id);
		echo json_encode($data);
	}


	public function ajax_add()
	{
		$this->_validate(true);

		$data = array(
				'area_nombre' => $this->input->post('nombre'),
				'area_notas' => $this->input->post('notas'),

				//'area_conraza' => $this->input->post('conraza'),
				//'area_contamanios' => $this->input->post('contamanios'),
			);

		$insert = $this->area->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$this->_validate();

		$data = array(
				'area_nombre' => $this->input->post('nombre'),
				'area_notas' => $this->input->post('notas'),
				//'area_conraza' => $this->input->post('conraza'),
				//'animal_contamanios' => $this->input->post('contamanios'),
			);

		$this->area->update(array('area_id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$data = array();

		$resultado = $this->area->delete_by_id($id);

		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}

		// Valida que no exista un registro con el mismo nombre
		if ($add)
			$duplicated = $this->area->check_duplicated(trim($this->input->post('nombre')));
		else
			$duplicated = $this->area->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ya existe un Area con ese nombre';
			$data['status'] = FALSE;
		}	

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}