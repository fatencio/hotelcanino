<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Box extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/box_model','box');
		$this->load->model('ABM/area_model','area');

		$this->load->helper('url');	
		$this->load->library('validations');	
	}


	public function index()
	{
		$datos_vista = array();

		$datos_vista["areas"] = $this->area->get_all();

		$this->load->view('ABM/box_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->box->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $box) {
			$no++;
			$row = array();

			$row[] = $box->nombre;
     		//$row[] = $box->area_asignada;
     		$row[] = $box->box_area_asignada_nombre;


			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_box('."'".$box->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_box('."'".$box->nombre."'".',' .$box->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->box->count_all(),
						"recordsFiltered" => $this->box->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->box->get_by_id($id);

		echo json_encode($data);
	}


	public function ajax_add()
	{
		$this->_validate(true);

		$data = array(
				'box_nombre' => $this->input->post('nombre_box'),
				'box_capacidad' => $this->input->post('capacidad_box'),
				'box_notas' => $this->input->post('notas_box'),
				'box_area_id' => $this->input->post('area_id'),
			);

		$insert = $this->box->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$this->_validate();

		$data = array(
				'box_nombre' => $this->input->post('nombre_box'),
				'box_capacidad' => $this->input->post('capacidad_box'),
				'box_notas' => $this->input->post('notas_box'),
				'box_area_id' => $this->input->post('area_id'),				
			);

		$this->box->update(array('box_id' => $this->input->post('id_box')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{   
		$data = array();

		$resultado = $this->box->delete_by_id($id);
		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre_box')) == '')
		{
			$data['inputerror'][] = 'nombre_box';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}		

        if($this->input->post('area_id') == '')
		{
			$data['inputerror'][] = 'area_id';
			$data['error_string'][] = 'Seleccione un aarea';
			$data['status'] = FALSE;
		}
		else
		{
			// Valida que no exista un registro con el mismo nombre
			if ($add)
				$duplicated = $this->box->check_duplicated(trim($this->input->post('nombre_box')), $this->input->post('area_id'));
			else
				$duplicated = $this->box->check_duplicated_edit($this->input->post('id_box'), trim($this->input->post('nombre_box')), $this->input->post('area_id'));

			if ($duplicated > 0)
			{
				$data['inputerror'][] = 'nombre_box';
				$data['error_string'][] = 'Ya existe un box con ese nombre';
				$data['status'] = FALSE;
			}	
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	public function get_by_area($area_id)
	{
		$data = $this->presentacion->get_by_area($area_id);

        echo json_encode($data);
	}	
}