<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mascota extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/mascota_model','mascota');
		$this->load->model('Tools/media_model','media');	
		$this->load->model('ABM/animal_model','animal');
	    $this->load->model('Usuarios/cliente_model','cliente');
        $this->load->model('ABM/raza_model','raza');
        $this->load->model('ABM/tamanio_model','tamanio');

        $this->load->library('validations');
	}


	public function index()
	{
		$this->load->helper('url');

		$datos_vista["animales"] = $this->animal->get_all();
        $datos_vista["clientes"] = $this->cliente->get_all();
        $datos_vista["razas"] = $this->raza->get_all();
        $datos_vista["tamanios"] = $this->tamanio->get_all();

		$this->load->view('ABM/mascota_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->mascota->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $mascota) {

			$checked = $mascota->status == 1 ? "checked": "";
			$status = $mascota->status == 1 ? 0:1;

			$no++;
			$row = array();

			$row[] = $mascota->nombre;
            $row[] = $mascota->cliente;			
            $row[] = $mascota->animal;
            $row[] = $mascota->notas;
        


			$row[] = '<img src="'. IMG_PATH . 'mascotas/miniaturas/'. $mascota->avatar .'" width="100">';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_mascota('."'".$mascota->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>

				<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_mascota('."'".$mascota->nombre."'".',' .$mascota->id. ')"><i class="glyphicon glyphicon-trash"></i></a>&nbsp;&nbsp;

			    <label class="css-input switch switch-sm switch-info" title="Activar / Desactivar">
			 	<input type="checkbox" name="switch_mascota"' . $checked . ' onclick="switch_mascota('. $mascota->id .')"><span></span>
              	</label>';
		
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->mascota->count_all(),
						"recordsFiltered" => $this->mascota->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->mascota->get_by_id($id);
		echo json_encode($data);
	}


	public function ajax_activar_desactivar($id)
	{
		if (isset($_SESSION['username']))
		{
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$nuevo_status = 0;
			$mascota = $this->mascota->get_by_id($id);

			if($mascota->status == 0) $nuevo_status = 1;

			 if ($nuevo_status == 0)
			 {
			 	$data = array(
			 		'mascota_status' => $nuevo_status,

					// Audit trail
					'mascota_fecha_suspension' => $ahora->format('Y-m-d H:i:s'),
					'mascota_usuario_suspension' => $_SESSION['username']			
				);	
			}
			else
			{
				$data = array(
					'mascota_status' => $nuevo_status,

					// Audit trail
					'mascota_fecha_habilitacion' => $ahora->format('Y-m-d H:i:s'),
					'mascota_usuario_habilitacion' => $_SESSION['username']			
				);	
			}

			$this->mascota->update(array('mascota_id' => $id), $data);

			echo json_encode(array("status" => TRUE, "nuevo_status" => $nuevo_status));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}
	}


	public function ajax_add()
	{
		if (isset($_SESSION['username']))
		{
			$imagen = '';
			
			$this->_validate(true);
			

			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');
				
				$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'mascotas');

				
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen');		
			}

            $fecha_nac = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('nacimiento')))));
			$fecha_fall = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('fallecimiento')))));
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));		

			$data = array(
					'mascota_nombre' => $this->input->post('nombre'),
					'mascota_animal_id' => $this->input->post('animal_id'),
					'mascota_nacimiento' => $fecha_nac,
					'mascota_raza_id' => $this->input->post('raza_id'),
					'mascota_sexo' => $this->input->post('sexo'),
					'mascota_pelaje' => $this->input->post('pelaje'),
					'mascota_tamanio_id' =>  $this->input->post('tamanio_id'),
					'mascota_pesoingreso' =>  $this->input->post('pesoingreso'),
					'mascota_pesosalida' =>  $this->input->post('pesosalida'),
					'mascota_foto' => $imagen,
                    'mascota_cliente_id' => $this->input->post('cliente_id'),
                    'mascota_notas' =>  $this->input->post('notas'),
                    'mascota_status' =>  1
           
                     );


					// Audit trail
					//'mascota_fecha_alta' => $ahora->format('Y-m-d H:i:s'),
					//'mascota_usuario_alta' => $_SESSION['username']
				

			$insert = $this->mascota->save($data);
            	

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}




	public function ajax_update()
	{
		if (isset($_SESSION['username']))
		{		
			$this->_validate();

			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');

				$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'mascotas');
				
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen');		
			}	

            $fecha_nac = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('nacimiento')))));
		    $fecha_fall = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('fallecimiento')))));
			
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			
             $data = array(
					'mascota_nombre' => $this->input->post('nombre'),
					'mascota_animal_id' => $this->input->post('animal_id'),
					'mascota_nacimiento' =>  $this->input->post('nacimiento'),
					'mascota_nacimiento' =>  $fecha_nac,
					'mascota_fallecimiento' =>  $fecha_fall,
					'mascota_raza_id' => $this->input->post('raza_id'),
					'mascota_sexo' => $this->input->post('sexo'),
					'mascota_pelaje' => $this->input->post('pelaje'),
					'mascota_tamanio_id' => $this->input->post('tamanio_id'),
					'mascota_pesoingreso' =>  $this->input->post('pesoingreso'),
					'mascota_pesosalida' =>  $this->input->post('pesosalida'),
					'mascota_foto' => $imagen,
                    'mascota_cliente_id' => $this->input->post('cliente_id'),
                    'mascota_notas' => $this->input->post('notas'),

        





					// Audit trail
				//	'cliente_fecha_modificacion' => $ahora->format('Y-m-d H:i:s'),
					//'cliente_usuario_modificacion' => $_SESSION['username']				
				);

			$this->mascota->update(array('mascota_id' => $this->input->post('id')), $data);

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}

	public function ajax_update_password()
	{
		$this->_validate_password();

		$data = array(
				'mascota_password' => $this->input->post('password'),

			);

		$this->mascota->update_password(array('mascota_id' => $this->input->post('mascota_id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	// Elimina Mascota solo si no tiene Ventas o Turnos asociados
	
	public function ajax_delete($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$this->mascota->update(array('mascota_id' => $id));

				$data['status'] = true;
	       
			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}



	public function ajax_delete_full($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$this->mascota->delete_by_id ($id);

			$data['status'] = true;
	       
			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}
	

	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}

	    if(trim($this->input->post('animal_id')) == '')
		{
			$data['inputerror'][] = 'animal_id';
			$data['error_string'][] = 'Ingrese un tipo de animal';
			$data['status'] = FALSE;
		}
		
		if(trim($this->input->post('nacimiento')) == '')
		{
			$data['inputerror'][] = 'nacimiento';
			$data['error_string'][] = 'Ingrese una fecha de nacimiento';
			$data['status'] = FALSE;
		}								

 	    if(trim($this->input->post('raza_id')) == '')
		{
			$data['inputerror'][] = 'raza_id';
			$data['error_string'][] = 'Ingrese raza';
			$data['status'] = FALSE;
		}			
		
		if(trim($this->input->post('sexo')) == 'Sin Sexo')
		{
			$data['inputerror'][] = 'sexo';
			$data['error_string'][] = 'Ingrese sexo';
			$data['status'] = FALSE;
		}			

		if(trim($this->input->post('cliente_id')) == 'Seleccione cliente...')
		{
			$data['inputerror'][] = 'cliente_id';
			$data['error_string'][] = 'Ingrese cliente';
			$data['status'] = FALSE;
		}	
	
			

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			if(!$this->media->check_image_extension($_FILES['foto']['name'])){

				$data['inputerror'][] = 'foto';
				$data['error_string'][] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$data['status'] = FALSE;				
			}
		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_password()
	{
		// $data = array();
		// $data['error_string'] = array();
		// $data['inputerror'] = array();
		// $data['status'] = TRUE;
	

		// if(trim($this->input->post('password')) == '')
		// {
		// 	$data['inputerror'][] = 'password';
		// 	$data['error_string'][] = 'Ingrese nueva contraseña';
		// 	$data['status'] = FALSE;
		// }	

		// if(trim($this->input->post('password2')) == '')
		// {
		// 	$data['inputerror'][] = 'password2';
		// 	$data['error_string'][] = 'Repita la nueva contraseña';
		// 	$data['status'] = FALSE;
		// }	

		// // Valida longitud de la contraseña
		// if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
		// {
		// 	$data['inputerror'][] = 'password';
		// 	$data['error_string'][] = 'Longitud mínima de la contraseña: 6 caracteres';
		// 	$data['status'] = FALSE;
		// }

		// // Valida contraseñas iguales
		// if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
		// {
		// 	if(trim($this->input->post('password')) != trim($this->input->post('password2')))
		// 	{
		// 		$data['inputerror'][] = 'password2';
		// 		$data['error_string'][] = 'Las contraseñas no coinciden';
		// 		$data['status'] = FALSE;
		// 	}
		// }	


		// if($data['status'] === FALSE)
		// {
		// 	echo json_encode($data);
		// 	exit();
		// }
	}


	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en algúna mascota
		$count = $this->mascota->check_imagen($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'mascota')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen de mascota.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);

	}	
}