<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios/cliente_model','cliente');
		$this->load->model('Tools/provincia_model','provincia');	
		$this->load->model('Tools/departamento_model','departamento');	
		$this->load->model('Tools/media_model','media');	

        $this->load->library('validations');
	}


	public function index()
	{
		$this->load->helper('url');
		$this->load->library('form_validation'); 

		$datos_vista["provincias"] = $this->provincia->get_all();
		$datos_vista["departamentos"] = $this->departamento->get_all();

		$this->load->view('Usuarios/cliente_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->cliente->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $cliente) {

			$checked = $cliente->status == 1 ? "checked": "";
			$status = $cliente->status == 1 ? 0:1;

			$no++;
			$row = array();

			$row[] = $cliente->nombre;
            $row[] = $cliente->apellido;
            $row[] = $cliente->telefono;
            $row[] = $cliente->email;
			
			//add html for action
			$row[] = '

				<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_cliente('."'".$cliente->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>

				<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_cliente('."'".$cliente->nombre."'".',' .$cliente->id. ')"><i class="glyphicon glyphicon-trash"></i></a>&nbsp;&nbsp;

			 	<label class="css-input switch switch-sm switch-info" title="Activar / Desactivar">
			 		<input type="checkbox" name="switch_cliente"' . $checked . ' onclick="switch_cliente('. $cliente->id .')"><span></span>
             	</label>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->cliente->count_all(),
						"recordsFiltered" => $this->cliente->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->cliente->get_by_id($id);
		echo json_encode($data);
	}


	public function ajax_activar_desactivar($id)
	{
		if (isset($_SESSION['username']))
		{
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$nuevo_status = 0;
			$cliente = $this->cliente->get_by_id($id);

			if($cliente->status == 0) $nuevo_status = 1;

			if ($nuevo_status == 0)
			{
				$data = array(
					'cliente_status' => $nuevo_status,

					// Audit trail
					'cliente_fecha_suspension' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_suspension' => $_SESSION['username']			
				);	
			}
			else
			{
				$data = array(
					'cliente_status' => $nuevo_status,

					// Audit trail
					'cliente_fecha_habilitacion' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_habilitacion' => $_SESSION['username']			
				);	
			}

			$this->cliente->update(array('cliente_id' => $id), $data);

			echo json_encode(array("status" => TRUE, "nuevo_status" => $nuevo_status));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}
	}


	public function ajax_add()
	{
		if (isset($_SESSION['username']))
		{
			
			$this->_validate(true);
			
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));		

			$data = array(
					'cliente_nombre' => $this->input->post('nombre'),
					'cliente_apellido' => $this->input->post('apellido'),
					'cliente_telefono' => $this->input->post('telefono'),
					'cliente_email' => $this->input->post('email'),
					'cliente_direccion' => $this->input->post('direccion'),
					'cliente_localidad' => $this->input->post('localidad'),
					'cliente_departamento_id' => $this->input->post('departamento'),
					'cliente_provincia_id' => '13',  //TODO:pasar id de provincia cuando sea multiprovincia
                    'cliente_dni' => $this->input->post('dni'),
                    'cliente_notas' =>  $this->input->post('notas'),
                    'cliente_telefono2' =>  $this->input->post('telefono2'),



					// Audit trail
					'cliente_fecha_alta' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_alta' => $_SESSION['username']
				);

			$insert = $this->cliente->save($data);

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	public function ajax_update()
	{
		if (isset($_SESSION['username']))
		{		
			$this->_validate();

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$data = array(
					'cliente_nombre' => $this->input->post('nombre'),
					'cliente_apellido' => $this->input->post('apellido'),
					'cliente_telefono' => $this->input->post('telefono'),
					'cliente_email' => $this->input->post('email'),
					'cliente_direccion' => $this->input->post('direccion'),
					'cliente_localidad' => $this->input->post('localidad'),
					'cliente_departamento_id' => $this->input->post('departamento'),
					'cliente_provincia_id' => '13',  //TODO:pasar id de provincia cuando sea multiprovincia
				    'cliente_dni' => $this->input->post('dni'),
                    'cliente_notas' =>  $this->input->post('notas'),
                    'cliente_telefono2' =>  $this->input->post('telefono2')

					// Audit trail
				//	'cliente_fecha_modificacion' => $ahora->format('Y-m-d H:i:s'),
					//'cliente_usuario_modificacion' => $_SESSION['username']				
				);

			$this->cliente->update(array('cliente_id' => $this->input->post('id')), $data);

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}

	

	// Elimina Cliente 
	
	public function ajax_delete($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

	        // Valida si hay VENTAS del cliente
	        $ventas = $this->cliente->count_ventas($id);

	        // Valida si hay TURNOS del cliente
	        $turnos = $this->cliente->count_turnos($id);

	        if ($ventas == 0 && $turnos == 0)
	        {
				$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

				$dataDelete = array(

						// Audit trail
						'cliente_fecha_baja' => $ahora->format('Y-m-d H:i:s'),
						'cliente_usuario_baja' => $_SESSION['username']				
					);

				$this->cliente->update(array('cliente_id' => $id), $dataDelete);

				$data['status'] = true;
	        }
	        else
	        {
	        	$data['status'] = false;
	        	$data['mensaje'] = "No es posible eliminar este elemento ya que está siendo utilizado.";
	        }

			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}
	

	// Elimina Cliente junto con Ventas o Turnos asociados
	
	public function ajax_delete_full($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

	        $ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$dataDelete = array(
					// Audit trail
					'cliente_fecha_baja' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_baja' => $_SESSION['username']				
				);

	        $this->db->trans_begin();

	       
	        // Elimina CLIENTE
			$this->cliente->update(array('cliente_id' => $id), $dataDelete);

			$this->db->trans_commit();

			$data['status'] = true;

			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}
	

	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}

	    if(trim($this->input->post('apellido')) == '')
		{
			$data['inputerror'][] = 'apellido';
			$data['error_string'][] = 'Ingrese un Apellido';
			$data['status'] = FALSE;
		}
		
		if(trim($this->input->post('email')) == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail';
			$data['status'] = FALSE;
		}								

 	    if(trim($this->input->post('dni')) == '')
		{
			$data['inputerror'][] = 'dni';
			$data['error_string'][] = 'Ingrese un dni';
			$data['status'] = FALSE;
		}	
         


 	    if  (!is_numeric(trim($this->input->post('dni'))))
		{
			$data['inputerror'][] = 'dni';
			$data['error_string'][] = 'Ingrese un dni con números';
			$data['status'] = FALSE;
		}	 


 	    if  (strlen(trim($this->input->post('dni'))) < 7)
		{
			$data['inputerror'][] = 'dni';
			$data['error_string'][] = 'Ingrese un dni de longitud válida';
			$data['status'] = FALSE;
		}	 







		// Valida formato email
		if(!filter_var(trim($this->input->post('email')), FILTER_VALIDATE_EMAIL))
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail válido';
			$data['status'] = FALSE;
		}	

		// Valida que no exista una entidad con el mismo email (Cliente o Local)
		if ($add)
			$duplicated = $this->cliente->check_duplicated(trim($this->input->post('email')));
		else
			$duplicated = $this->cliente->check_duplicated_edit($this->input->post('id'), trim($this->input->post('email')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ya existe un usuario con ese e-mail';
			$data['status'] = FALSE;
		}	

        if  (strlen(trim($this->input->post('departamento'))) < 1)
		{
			$data['inputerror'][] = 'departamento';
			$data['error_string'][] = 'Ingrese un departamento';
			$data['status'] = FALSE;
		}	 

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
	
}