<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reserva extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/reserva_model','reserva');
		$this->load->model('ABM/mascota_model','mascota');
		$this->load->model('ABM/animal_model','animal');
		$this->load->model('ABM/area_model','area');
		$this->load->model('ABM/box_model','box');

        $this->load->library('validations');

        $this->load->helper('url');
	}


	public function index()
	{
		$datos_vista = array();

		$datos_vista["mascotas"] = $this->mascota->get_all(TRUE);
		$datos_vista["animales"] = $this->animal->get_all();
		$datos_vista["areas"] = $this->area->get_all();
		$datos_vista["boxes"] = $this->box->get_all_with_area();

		$this->load->view('Actividad/reserva_view', $datos_vista);		
	}


	function ajax_areas()
	{
		$areas = array();

		$arrAreas = $this->area->get_all();

		// Agrega Area ficticia para asignar reservas pendientes (sin box asignado)
		$a = array();
        $a['id'] = -1;
        $a['title'] = ' SIN BOX ASIGNADO';

        array_push($areas, $a);

		if(sizeof($arrAreas) > 0)
		{ 
			foreach($arrAreas as $area)
			{
		        $a = array();
		        $boxes = array();

		        // Lista de boxes del area actual
		        $arrBoxes = $this->box->get_by_area($area->id);

				foreach($arrBoxes as $box)
				{
					$b = array();

			        $b['id'] = $box->id;
			        $b['title'] = $box->nombre;		        

			        array_push($boxes, $b);					
				}

		        $a['id'] = $area->id;
		        $a['title'] = ' ' . $area->nombre;
		        $a['children'] = $boxes;

		        array_push($areas, $a);
			}
		}	

	    // Output json for our calendar
	    echo json_encode($areas);		
	}


	function ajax_reservas()
	{
		$reservas = array();
	    $colorConIngreso = '#3BA9E8';
     	$colorSinIngreso = '#37A967';

		$arrReservas = $this->reserva->get_all();

		if(sizeof($arrReservas) > 0)
		{ 
			foreach($arrReservas as $reserva)
			{
		        $r = array();

		        $r['id'] = $reserva->id;
		        $r['resourceId'] = $reserva->box_id;
		        $r['title'] = ' ' . strtoupper($reserva->mascota) . ' (' . $reserva->cliente . ')';
		        $r['start'] = ' ' . $reserva->fecha_desde;
		        $r['end'] = ' ' . $reserva->fecha_hasta;

		        // Reservas con box asignado
		        if ($reserva->box_id != -1)
		        	$r['color'] = $colorConIngreso;
		        else
		        	$r['color'] = $colorSinIngreso;

		        array_push($reservas, $r);
			}
		}	

	    // Output json for our calendar
	    echo json_encode($reservas);		
	}


	public function ajax_list()
	{
		$list = $this->reserva->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $reserva) {

			$checked = $reserva->status == 1 ? "checked": "";
			$status = $reserva->status == 1 ? 0:1;

			$no++;
			$row = array();

			$row[] = $reserva->nombre;
            $row[] = $reserva->cliente;			
            $row[] = $reserva->animal;
            $row[] = $reserva->notas;
        


			$row[] = '<img src="'. IMG_PATH . 'reservas/miniaturas/'. $reserva->avatar .'" width="100">';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_reserva('."'".$reserva->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>

				<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_reserva('."'".$reserva->nombre."'".',' .$reserva->id. ')"><i class="glyphicon glyphicon-trash"></i></a>&nbsp;&nbsp;

			    <label class="css-input switch switch-sm switch-info" title="Activar / Desactivar">
			 	<input type="checkbox" name="switch_reserva"' . $checked . ' onclick="switch_reserva('. $reserva->id .')"><span></span>
              	</label>';
		
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->reserva->count_all(),
						"recordsFiltered" => $this->reserva->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->reserva->get_by_id($id);
		echo json_encode($data);
	}


	public function ajax_add()
	{
		if (isset($_SESSION['username']))
		{
			$imagen = '';
			
			$this->_validate(true);

            //Transformo a formato fecha
            $fecha_ingreso = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('ingreso')))));
			$fecha_egreso = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('egreso')))));

		    // Suma 23:59:59 a fecha egreso
		    $fecha_egreso = new DateTime($fecha_egreso); 
		    $fecha_egreso = $fecha_egreso->add(new DateInterval('PT23H59M59S')); 
		    $fecha_egreso = $fecha_egreso->format('Y-m-d H:i:s');

			// Si no se seleccionó box, lo inserta como NULL
			$box = $this->input->post('box_id') == '' ? NULL : $this->input->post('box_id');

			$data = array(
					'reserva_mascota_id' => $this->input->post('mascota_id'),
					'reserva_fecha_desde' => $fecha_ingreso,
					'reserva_fecha_hasta' => $fecha_egreso,
					'reserva_estado' => $this->input->post('estado'), 
					'reserva_box_id' => $box, 
					'reserva_notas' =>  $this->input->post('notas')
					);
				
			$insert = $this->reserva->save($data);
            	
			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}




	public function ajax_update()
	{
		if (isset($_SESSION['username']))
		{		
			$this->_validate();

            //Transformo a formato fecha
            $fecha_ingreso = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('ingreso')))));
			$fecha_egreso = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('egreso')))));

		    // Suma 23:59:59 a fecha egreso
		    $fecha_egreso = new DateTime($fecha_egreso); 
		    $fecha_egreso = $fecha_egreso->add(new DateInterval('PT23H59M59S')); 
		    $fecha_egreso = $fecha_egreso->format('Y-m-d H:i:s');
		    
			// Si no se seleccionó box, lo inserta como NULL
			$box = $this->input->post('box_id') == '' ? NULL : $this->input->post('box_id');

			$data = array(
					'reserva_mascota_id' => $this->input->post('mascota_id'),
					'reserva_fecha_desde' => $fecha_ingreso,
					'reserva_fecha_hasta' => $fecha_egreso,
					'reserva_estado' => $this->input->post('estado'), 
					'reserva_box_id' => $box, 
					'reserva_notas' =>  $this->input->post('notas')
					);

			$this->reserva->update(array('reserva_id' => $this->input->post('id')), $data);

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}




	// Elimina reserva solo si no tiene Ventas o Turnos asociados
	public function ajax_delete($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

			$this->reserva->update(array('reserva_id' => $id));

				$data['status'] = true;
	       
			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}



	public function ajax_delete_full($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

			$this->reserva->delete_by_id ($id);

			$data['status'] = true;
	       
			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}
	


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;


	    if(trim($this->input->post('animal_id')) == '')
		{
			$data['inputerror'][] = 'animal_id';
			$data['error_string'][] = 'Seleccione un tipo de animal';
			$data['status'] = FALSE;
		}
		

	    if(trim($this->input->post('mascota_id')) == '')
		{
			$data['inputerror'][] = 'mascota_id';
			$data['error_string'][] = 'Seleccione una mascota';
			$data['status'] = FALSE;
		}
		
		if(trim($this->input->post('ingreso')) == '')
		{
			$data['inputerror'][] = 'ingreso';
			$data['error_string'][] = 'Seleccione una fecha de ingreso';
			$data['status'] = FALSE;
		}								

 	   	if(trim($this->input->post('egreso')) == '')
		{
			$data['inputerror'][] = 'egreso';
			$data['error_string'][] = 'Seleccione una fecha de egreso';
			$data['status'] = FALSE;
		}						
		
		if(trim($this->input->post('estado')) == '')
		{
			$data['inputerror'][] = 'estado';
			$data['error_string'][] = 'Seleccione un estado';
			$data['status'] = FALSE;
		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}