-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2018 a las 16:54:18
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hotelcaninonueva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `animal`
--

CREATE TABLE `animal` (
  `animal_id` int(11) NOT NULL,
  `animal_nombre` varchar(255) DEFAULT NULL,
  `animal_conraza` tinyint(4) NOT NULL DEFAULT '0',
  `animal_contamanios` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `animal`
--

INSERT INTO `animal` (`animal_id`, `animal_nombre`, `animal_conraza`, `animal_contamanios`) VALUES
(1, 'Perro', 1, 1),
(2, 'Gato', 1, 0),
(4, 'Otro', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `area_id` int(11) NOT NULL,
  `area_nombre` varchar(200) NOT NULL,
  `area_notas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`area_id`, `area_nombre`, `area_notas`) VALUES
(2, 'Gatos', 'Mejorar cierre'),
(3, 'Perros Grandes', 'Falta mediasombra'),
(5, 'Casitas Afuera', ''),
(6, 'Perros Pequeños', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `box`
--

CREATE TABLE `box` (
  `box_id` int(11) NOT NULL,
  `box_nombre` varchar(100) NOT NULL,
  `box_capacidad` int(11) NOT NULL,
  `box_lugares_ocupados` int(11) NOT NULL,
  `box_area_id` int(11) NOT NULL,
  `box_notas` text NOT NULL,
  `box_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `box`
--

INSERT INTO `box` (`box_id`, `box_nombre`, `box_capacidad`, `box_lugares_ocupados`, `box_area_id`, `box_notas`, `box_status`) VALUES
(2, 'Gatos 1', 2, 1, 2, 'Limpiar', 1),
(3, 'Gatos 2', 4, 4, 2, '', 1),
(4, 'Gatos 3', 1, 0, 2, '', 1),
(5, 'Grandes 1', 1, 0, 3, 'Arreglar cierre', 1),
(6, 'Casita 1', 1, 0, 5, '', 1),
(7, 'Casita 2', 1, 0, 5, '', 1),
(8, 'Casita 3', 1, 0, 5, 'Comprar casa nueva', 1),
(9, 'Peques 1', 3, 0, 6, '', 1),
(10, 'Peques 2', 2, 0, 6, '', 1),
(11, 'Peques 3', 2, 0, 6, 'Mejorar piso', 1),
(12, 'Peques 4', 2, 0, 6, '', 1),
(13, 'Test 3', 2, 0, 3, '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('037a59da8bbcf55b3be30b1b83e1a889270fcc86', '200.41.186.154', 1465767771, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353736373737313b),
('09a3964082d9741d53ccc5fe77b1cf541ae8b00f', '190.30.71.117', 1465912278, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353931323032313b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('14c0525a358814dd6cb924eaaa89c5113c85706c', '190.30.71.117', 1465771460, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353737313435383b),
('1ef27cf9c9758e3ec5b08abb64a6a45351696789', '181.169.189.200', 1465674015, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637343031353b),
('208cac88cd2a729139a74481e05e0fbaf8aa67d5', '190.30.71.117', 1465677756, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637373636303b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('24b7a89928b46eb0ede41cf1ad71c09519f3f74b', '190.216.7.213', 1465848297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353834383239373b),
('433c954678e512617af047e929aeb97dcf29b96b', '200.126.242.55', 1465854259, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353835343235393b),
('4e70df7a8b3529d1ccb6c2ffa67db83248e92468', '200.41.186.154', 1465750960, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353735303936303b),
('565eaca5930029254771ac70dddb788b1b51654c', '190.30.71.117', 1465831721, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353832393531383b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('5ab03fd38aa382b4dbf1957c02990b300dbb8f6b', '190.216.7.213', 1466087633, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363038373632353b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('60688689106a81cea7e6ff7ea60b742046d1ddfc', '181.169.189.200', 1465916905, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353931363930353b),
('635e01be14baf67cb9826dc58de9ae67876d6e27', '200.41.186.154', 1465708940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353730383934303b),
('66d719ca9a2698a955ad5b836875bb7ec421b253', '190.216.7.213', 1466033993, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363033333032383b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('69e3e1e7f31f2b98d268925104518cbfa631af8f', '200.41.186.154', 1465776012, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353737363031323b),
('6bdcd9322f6c25c9d47ec1b9a43153179139f677', '181.14.129.124', 1466020691, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363032303639313b),
('6e963dea58aa8f25f86db929e516590a00fd98ad', '190.216.7.213', 1466090475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363039303437353b),
('727d8444cae268d08a2c26706df3e3c601d61137', '190.216.7.213', 1465998319, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353939383331393b),
('7a41617eb77c194e9ebcd23cc7a400d2813a348e', '181.14.129.124', 1466046035, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363034363033353b),
('962fcdfb34a55e82f1e17b3eff275898894df064', '190.216.7.213', 1466014050, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363031343034313b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('9a633e0afca2ba91700239a0a29d125e445fc9a2', '190.30.71.117', 1465915256, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353931353133313b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('9b80f36a9db437bd0bd41321a208fc64b0ab29c6', '181.14.129.124', 1466046873, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363034363837333b),
('9ec3e518a32d1f59a664f854b88ea31b9c61a0fd', '190.216.7.213', 1466030057, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363033303035373b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('a8a8507cb90eab4675652b39fd5684be48fdcbe8', '200.41.186.154', 1465748734, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353734383733343b),
('ab87d84fe1e633b49244f319c0af418153ef2b44', '190.30.71.117', 1465672285, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637313336323b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('b98f542fedc544983bf8e7c1fb0b8dd55782b6f1', '190.30.71.117', 1465831822, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353833313733323b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('bdca31feb061a4e8a41f36b13dfe08d4d6ae4a52', '181.169.189.200', 1465674190, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637343139303b),
('beb3bdbd51cd8ae3742a99f01e557da6f20888f0', '200.41.186.154', 1465745975, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353734353937353b),
('c4db65d97aa04c027d001a3a17bf0e9a19bb8bec', '181.14.129.124', 1466095072, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363039343938393b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('c9dc0a26aa1f277c8466dd4d850c050cbd937674', '200.41.186.154', 1465707940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353730373934303b),
('d58c5d30b442af5a0ca23c490fe9537736d0ec3a', '190.30.71.117', 1465674057, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637323334343b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('d6edaeef83bbe63d659f2033e120c8b7c36f01fb', '190.216.7.213', 1466092964, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363039323936343b),
('dc636a83dabc80ad7bdbf554fd3cc4271b93c9db', '181.14.129.124', 1466039984, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363033393936363b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('e8631833c456bdd81d23017bc69f4d786ae502bc', '181.169.189.200', 1465667168, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353636363631343b757365725f69647c693a31353b757365726e616d657c733a353a224776696465223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('eb4324b85ebf7f88b4fc41270d3d5d78754dfcc5', '190.216.7.213', 1466029599, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363032393431313b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('efb0a80298b2d09b9fb8b3361c089f3d5544d5ab', '190.216.7.213', 1465822860, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353832323138333b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('f54ce72f90dfa49ff0e5a6ae6e9628cbb3da7682', '190.216.7.213', 1466018272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363031343437353b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('f6453b4633fad7f6759b0aee8a9b3db9d3608011', '181.169.189.200', 1465667467, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353636373436373b),
('fcb1362975fcd72c9b3884475e4b647ebf0c7644', '190.30.71.117', 1465671183, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353636363730313b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `cliente_id` int(11) NOT NULL,
  `cliente_dni` int(11) NOT NULL,
  `cliente_nombre` text NOT NULL,
  `cliente_email` text NOT NULL,
  `cliente_apellido` text NOT NULL,
  `cliente_telefono` text NOT NULL,
  `cliente_telefono2` text NOT NULL,
  `cliente_direccion` text NOT NULL,
  `cliente_localidad` text NOT NULL,
  `cliente_departamento_id` int(11) NOT NULL,
  `cliente_provincia_id` int(11) NOT NULL,
  `cliente_status` int(11) NOT NULL DEFAULT '1',
  `cliente_notas` text NOT NULL,
  `cliente_fecha_alta` datetime NOT NULL,
  `cliente_usuario_alta` varchar(200) NOT NULL,
  `cliente_usuario_modificacion` varchar(200) NOT NULL,
  `cliente_fecha_baja` datetime NOT NULL,
  `cliente_usuario_baja` varchar(200) NOT NULL,
  `cliente_fecha_suspension` datetime NOT NULL,
  `cliente_usuario_suspension` varchar(200) NOT NULL,
  `cliente_fecha_habilitacion` datetime NOT NULL,
  `cliente_usuario_habilitacion` varchar(200) NOT NULL,
  `cliente_activacion` varchar(64) NOT NULL,
  `cliente_fecha_activacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`cliente_id`, `cliente_dni`, `cliente_nombre`, `cliente_email`, `cliente_apellido`, `cliente_telefono`, `cliente_telefono2`, `cliente_direccion`, `cliente_localidad`, `cliente_departamento_id`, `cliente_provincia_id`, `cliente_status`, `cliente_notas`, `cliente_fecha_alta`, `cliente_usuario_alta`, `cliente_usuario_modificacion`, `cliente_fecha_baja`, `cliente_usuario_baja`, `cliente_fecha_suspension`, `cliente_usuario_suspension`, `cliente_fecha_habilitacion`, `cliente_usuario_habilitacion`, `cliente_activacion`, `cliente_fecha_activacion`) VALUES
(3, 0, 'Carlos', 'crodriguez@homail.com', 'Rodriguez', '11223344', '', 'madero 1474', 'San Fernando', 4, 13, 0, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '2017-08-08 11:44:00', 'fatencio', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(13, 0, 'Germán', 'Germanvans_@hotmail.com', 'Vide ', '15-1234-5678', '', 'Sarmiento 1590', 'San Fernando', 0, 0, 0, '', '0000-00-00 00:00:00', '', 'spiquet', '2017-09-06 16:00:11', 'Gvide', '2017-09-06 16:00:07', 'Gvide', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(14, 0, 'Sebastián', 'sebi_piquet@hotmail.com', 'Piquet', '4444-4444', '', 'Vergara 3646 1A', 'Florida Oeste', 0, 0, 0, '', '0000-00-00 00:00:00', '', '', '2017-09-06 16:00:41', 'Gvide', '2017-09-06 16:00:36', 'Gvide', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(17, 0, 'Juan', 'perro@gmail.com', 'Perez', '', '', '', '', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(28, 0, 'Christian', 'Turkototem@hotmail.com', 'Zanabria', '1111-1111', '', 'Concordia 5353', 'Villa devoto', 0, 0, 0, '', '0000-00-00 00:00:00', '', 'Gvide', '2017-09-06 15:59:42', 'Gvide', '2017-09-06 15:59:33', 'Gvide', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(30, 0, 'Fernando', 'fguzman@hotmail.com', 'Guzmán', '', '', '', '', 0, 0, 0, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '2017-08-09 18:32:48', 'Gvide', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(32, 0, 'Jhonatan', 'jbojacamarin93@gmail.com', 'Marin', '', '', '', '', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(35, 0, 'Francisco', 'fatencio@gmail.com', 'Atencio', '2615118372', '', '', '', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(39, 1233322, 'Elvira', 'elvirapiquet@hotmail.com', 'Guttierrez', '47991669', '', 'San Juan 200', 'Bermejo', 1, 13, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(41, 0, 'Florencia', 'florscopa@hola.com', 'Scopa', '', '', '', '', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '2017-08-06 14:18:32', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(42, 0, 'Manuel', 'casa@hotmail.com', 'Limp', '4745-4420', '', '', '', 0, 0, 0, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '2017-08-09 18:33:35', 'Gvide', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(45, 0, 'Natalia', 'mnataliameyer@gmail.com', 'Meyer', '1557490567', '', '', '', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(51, 0, 'Luciano', 'lucho@gmail.com', 'Pepe', '1111-1111', '', 'Monteverda 4208', 'Olivos', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(52, 0, 'Federico', 'fede@gmail.com', 'Piquet', '', '', '', '', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(53, 0, 'Luciana', 'lucianacmp@outlook.com', 'Martinez Plastina', '', '', '', '', 0, 0, 1, '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(75, 0, 'German', 'Germanvans_@hotmail.com', 'Vide', '4744-2222', '', 'Diaz Velez 650', 'La Lucila', 0, 0, 1, '', '2017-09-06 16:02:38', 'Germanvans_@hotmail.com', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(87, 0, 'Alejandro', 'alemr32@gmail.com', 'Rodriguez', '', '', '', '', 0, 0, 1, '', '2017-09-15 12:20:41', 'alemr32@gmail.com', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00'),
(110, 55555556, 'test ed', 'fatencio@gmail.comes', 'foto', '2615118372', '', 'calle', 'dist', 1, 13, 1, '', '2018-02-07 11:18:06', 'fatencio', '', '2018-02-07 11:19:10', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `departamento_id` int(11) NOT NULL,
  `departamento_nombre` varchar(200) NOT NULL,
  `departamento_provincia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`departamento_id`, `departamento_nombre`, `departamento_provincia_id`) VALUES
(1, 'Luján de Cuyo', 13),
(2, 'Maipú', 13),
(3, 'Lavalle', 13),
(4, 'Godoy Cruz', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascota`
--

CREATE TABLE `mascota` (
  `mascota_id` int(11) NOT NULL,
  `mascota_nombre` varchar(100) NOT NULL,
  `mascota_animal_id` int(11) NOT NULL,
  `mascota_nacimiento` date NOT NULL,
  `mascota_fallecimiento` date DEFAULT NULL,
  `mascota_raza_id` int(10) NOT NULL,
  `mascota_sexo` varchar(6) NOT NULL,
  `mascota_pelaje` varchar(100) NOT NULL,
  `mascota_tamanio_id` int(10) NOT NULL,
  `mascota_pesoingreso` int(11) NOT NULL,
  `mascota_pesosalida` int(11) NOT NULL,
  `mascota_foto` varchar(255) NOT NULL,
  `mascota_cliente_id` int(11) NOT NULL,
  `mascota_notas` text NOT NULL,
  `mascota_fecha_suspension` date NOT NULL,
  `mascota_usuario_suspension` varchar(50) NOT NULL,
  `mascota_fecha_habilitacion` date NOT NULL,
  `mascota_usuario_habilitacion` varchar(50) NOT NULL,
  `mascota_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mascota`
--

INSERT INTO `mascota` (`mascota_id`, `mascota_nombre`, `mascota_animal_id`, `mascota_nacimiento`, `mascota_fallecimiento`, `mascota_raza_id`, `mascota_sexo`, `mascota_pelaje`, `mascota_tamanio_id`, `mascota_pesoingreso`, `mascota_pesosalida`, `mascota_foto`, `mascota_cliente_id`, `mascota_notas`, `mascota_fecha_suspension`, `mascota_usuario_suspension`, `mascota_fecha_habilitacion`, `mascota_usuario_habilitacion`, `mascota_status`) VALUES
(2, 'Anka', 0, '2008-03-20', '0000-00-00', 1, 'hembra', 'Largo', 3, 0, 0, '', 2, '', '0000-00-00', '', '0000-00-00', '', 0),
(5, 'Samantha', 1, '2010-07-29', '0000-00-00', 1, '', 'Corto marrón con manchas negras y blancas', 3, 0, 0, '_500732421.jpg', 12, '', '2018-01-08', 'fatencio', '2018-01-08', 'fatencio', 0),
(13, 'Layla', 0, '2011-06-22', '0000-00-00', 1, 'hembra', 'Corto', 3, 0, 0, '_158020019.png', 13, '', '0000-00-00', '', '0000-00-00', '', 0),
(14, 'Flora', 1, '2012-01-01', '2018-02-02', 1, '', 'Largo', 3, 0, 0, '', 0, '', '0000-00-00', '', '0000-00-00', '', 0),
(24, 'Nina', 0, '2012-10-10', '0000-00-00', 1, 'hembra', 'rubia linda', 3, 0, 0, '_549072265.jpg', 3, '', '0000-00-00', '', '0000-00-00', '', 0),
(25, 'lila', 1, '2017-05-05', '2017-12-12', 1, 'Macho', 'negro y marron', 3, 0, 0, '', 12, '', '0000-00-00', '', '0000-00-00', '', 0),
(26, 'Pluto', 0, '2002-08-20', '0000-00-00', 1, 'macho', 'Largo', 3, 0, 0, '_453308105.jpg', 13, '', '0000-00-00', '', '0000-00-00', '', 0),
(27, 'Jorgelina', 28, '2017-01-05', '0000-00-00', 1, 'hembra', 'gris', 3, 0, 0, '', 14, '', '0000-00-00', '', '0000-00-00', '', 0),
(28, 'Virgina', 2, '2016-10-12', '0000-00-00', 1, 'Macho', 'blanco', 3, 0, 0, '_861175537.jpg', 2, '', '0000-00-00', '', '0000-00-00', '', 0),
(36, 'Luna', 1, '1970-01-01', '1970-01-01', 1, 'Hembra', 'Corto', 3, 0, 0, '43_166595458.JPG', 0, '', '0000-00-00', '', '2018-02-07', 'fatencio', 1),
(37, 'Luna', 4, '2017-02-09', '1970-01-01', 1, 'Hembra', 'Corto', 3, 0, 0, '43_166595458.JPG', 30, '', '0000-00-00', '', '2018-02-07', 'fatencio', 1),
(39, 'Batata', 1, '2016-12-29', '2017-12-29', 1, 'Macho', 'Corto', 3, 0, 0, '34_450450887.jpg', 35, 'Medicado:  Naproxeno 2 veces por dia', '0000-00-00', '', '2018-02-07', 'fatencio', 1),
(40, 'Luna', 1, '0000-00-00', '0000-00-00', 1, 'Hembra', '', 3, 0, 0, 'mascota_avatar_mini.png', 0, '', '0000-00-00', '', '0000-00-00', '', 0),
(41, 'Lina', 1, '1970-01-01', '1970-01-01', 1, 'Hembra', '', 3, 0, 0, '3_466941581.JPG', 39, 'No mezclar con perros pequeños', '0000-00-00', '', '2018-02-07', 'fatencio', 1),
(43, 'Samy', 2, '1970-01-01', '0000-00-00', 1, 'Macho', 'gris', 3, 10, 9, 'mascota_avatar_mini.png', 0, '', '0000-00-00', '', '0000-00-00', '', 0),
(44, 'Sami', 2, '2018-01-22', '1970-01-01', 1, 'Macho', 'Corto', 3, 10, 11, '9_78094482.jpg', 41, '', '0000-00-00', '', '2018-02-07', 'fatencio', 1),
(45, 'Sin CLiente', 1, '2018-01-25', '0000-00-00', 1, 'Macho', '', 3, 0, 0, 'mascota_avatar_mini.png', 0, '', '0000-00-00', '', '0000-00-00', '', 0),
(47, 'Lety', 1, '2018-01-17', '1970-01-01', 1, 'Hembra', '12', 3, 0, 0, '14_986599116.jpg', 45, '', '0000-00-00', '', '2018-02-07', 'fatencio', 1),
(48, 'status', 1, '2018-01-12', '0000-00-00', 1, 'Macho', 'ss', 3, 0, 0, 'mascota_avatar_mini.png', 1, '', '0000-00-00', '', '0000-00-00', '', 1),
(49, 'Cachila', 2, '2018-02-07', NULL, 1, 'Macho', 'Sucio', 3, 10, 10, '_111602783.png', 12, '', '0000-00-00', '', '0000-00-00', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `provincia_id` int(1) NOT NULL,
  `provincia_nombre` varchar(255) NOT NULL,
  `provincia_cpa` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`provincia_id`, `provincia_nombre`, `provincia_cpa`) VALUES
(1, 'Buenos Aires', 'B'),
(2, 'Catamarca', 'K'),
(3, 'Chaco', 'H'),
(4, 'Chubut', 'U'),
(5, 'Ciudad Autónoma de Buenos Aires', 'C'),
(6, 'Córdoba', 'X'),
(7, 'Corrientes', 'W'),
(8, 'Entre Ríos', 'E'),
(9, 'Formosa', 'P'),
(10, 'Jujuy', 'Y'),
(11, 'La Pampa', 'L'),
(12, 'La Rioja', 'F'),
(13, 'Mendoza', 'M'),
(14, 'Misiones', 'N'),
(15, 'Neuquén', 'Q'),
(16, 'Río Negro', 'R'),
(17, 'Salta', 'A'),
(18, 'San Juan', 'J'),
(19, 'San Luis', 'D'),
(20, 'Santa Cruz', 'Z'),
(21, 'Santa Fe', 'S'),
(22, 'Santiago del Estero', 'G'),
(23, 'Tierra del Fuego, Antártida e Islas del Atlántico Sur', 'V'),
(24, 'Tucumán', 'T');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `raza`
--

CREATE TABLE `raza` (
  `raza_id` int(11) NOT NULL,
  `raza_nombre` varchar(255) DEFAULT NULL,
  `raza_id_animal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `raza`
--

INSERT INTO `raza` (`raza_id`, `raza_nombre`, `raza_id_animal`) VALUES
(1, 'Mestizo', 1),
(2, 'Bulldog Francés', 1),
(3, 'Caniche Poodle', 1),
(4, 'Yorkshire Terrier', 1),
(5, 'Chihuahua ', 1),
(6, 'Dachshund Teckel', 1),
(7, 'Miniature Schnauzer', 1),
(8, 'Bulldog', 1),
(9, 'Ovejero Alemán', 1),
(10, 'Labrador Retriever ', 1),
(11, 'Golden Retriever', 1),
(12, 'Boxer', 1),
(13, 'Cocker Spaniel', 1),
(14, 'Shih Tzu', 1),
(15, 'Toy Poodle', 1),
(16, 'Rottweiler', 1),
(17, 'Siamese', 2),
(18, 'Persa', 2),
(19, 'Mestizo', 2),
(20, 'Terrier', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `reserva_id` int(11) NOT NULL,
  `reserva_mascota_id` int(11) NOT NULL,
  `reserva_box_id` int(11) DEFAULT NULL,
  `reserva_fecha_desde` date NOT NULL,
  `reserva_fecha_hasta` datetime NOT NULL,
  `reserva_estado` int(11) NOT NULL COMMENT '0: a confirmar   1:confirmada',
  `reserva_notas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`reserva_id`, `reserva_mascota_id`, `reserva_box_id`, `reserva_fecha_desde`, `reserva_fecha_hasta`, `reserva_estado`, `reserva_notas`) VALUES
(3, 39, 2, '2018-02-15', '2018-02-16 00:00:00', 1, ''),
(4, 39, 2, '2018-02-16', '2018-02-23 00:00:00', 0, 'con todo'),
(7, 44, NULL, '2018-02-17', '2018-03-02 00:00:00', 1, 'sin box'),
(8, 39, 4, '2018-02-23', '2018-03-01 00:00:00', 1, 'asigna gatos 1'),
(10, 39, 7, '2018-02-16', '2018-02-23 00:00:00', 0, ''),
(11, 44, 7, '2018-02-09', '2018-03-04 00:00:00', 1, ''),
(12, 47, NULL, '2018-02-16', '2018-03-16 00:00:00', 0, 'sdfasd'),
(13, 39, 6, '2018-02-21', '2018-02-21 00:00:00', 0, 'test'),
(14, 47, 7, '2018-03-04', '2018-03-07 00:00:00', 1, 'test hoy'),
(15, 47, NULL, '0000-00-00', '2018-03-01 00:00:00', 1, ''),
(16, 44, NULL, '2018-03-02', '2018-03-03 23:59:59', 1, '23:59'),
(17, 39, NULL, '2018-03-02', '2018-03-04 00:00:00', 1, 'con area, sin box'),
(18, 39, NULL, '2018-03-07', '2018-03-08 23:59:59', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reset`
--

CREATE TABLE `reset` (
  `reset_id` int(10) UNSIGNED NOT NULL,
  `reset_usuario_id` int(10) UNSIGNED NOT NULL,
  `reset_usuario_nombre` varchar(50) NOT NULL,
  `reset_usuario_tipo` varchar(10) NOT NULL,
  `reset_token` varchar(64) NOT NULL,
  `reset_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reset`
--

INSERT INTO `reset` (`reset_id`, `reset_usuario_id`, `reset_usuario_nombre`, `reset_usuario_tipo`, `reset_token`, `reset_creado`) VALUES
(5, 14, 'Sebas Usuario', 'cliente', '62875d817473d9393fa41f4994c0a842ad83a625', '2016-09-19 22:27:58'),
(8, 28, 'christian zanabria', 'cliente', '29ba6594635a037eb055d910859f64b797cfc73b', '2016-12-15 21:27:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tamanio`
--

CREATE TABLE `tamanio` (
  `tamanio_id` int(11) NOT NULL,
  `tamanio_nombre` varchar(255) DEFAULT NULL,
  `tamanio_animal_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tamanio`
--

INSERT INTO `tamanio` (`tamanio_id`, `tamanio_nombre`, `tamanio_animal_id`) VALUES
(3, 'Medianos', 1),
(4, 'Grandes', 1),
(17, 'Pequeños', 1),
(22, 'Gigantes', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) DEFAULT 'default.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_confirmed` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `nombre`, `apellido`, `username`, `email`, `password`, `avatar`, `created_at`, `updated_at`, `is_admin`, `is_confirmed`, `is_deleted`) VALUES
(10, 'Francisco', 'Atencio', 'fatencio', 'fatencio@gmail.com', '$2y$10$paoYN1RKPhwUPu8Ak6bXFeRzsKBSJ4IllXkCRdHBT6tLxn7hz/5pW', 'default.jpg', '2016-05-25 06:31:36', NULL, 1, 0, 0),
(17, 'Fabian', 'Acosta', 'facosta', 'hoteldemascotas@hotmail.com', '$2y$10$paoYN1RKPhwUPu8Ak6bXFeRzsKBSJ4IllXkCRdHBT6tLxn7hz/5pW', 'default.jpg', '0000-00-00 00:00:00', NULL, 0, 0, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `animal`
--
ALTER TABLE `animal`
  ADD PRIMARY KEY (`animal_id`);

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`area_id`);

--
-- Indices de la tabla `box`
--
ALTER TABLE `box`
  ADD PRIMARY KEY (`box_id`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cliente_id`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`departamento_id`);

--
-- Indices de la tabla `mascota`
--
ALTER TABLE `mascota`
  ADD PRIMARY KEY (`mascota_id`),
  ADD KEY `mascota_raza_id_2` (`mascota_raza_id`),
  ADD KEY `mascota_tamanio_id` (`mascota_tamanio_id`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`provincia_id`);

--
-- Indices de la tabla `raza`
--
ALTER TABLE `raza`
  ADD PRIMARY KEY (`raza_id`),
  ADD KEY `raza_id_animal` (`raza_id_animal`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`reserva_id`),
  ADD KEY `reserva_box_id` (`reserva_box_id`),
  ADD KEY `reserva_mascota_id` (`reserva_mascota_id`);

--
-- Indices de la tabla `reset`
--
ALTER TABLE `reset`
  ADD PRIMARY KEY (`reset_id`),
  ADD UNIQUE KEY `reset_usuario_id` (`reset_usuario_id`);

--
-- Indices de la tabla `tamanio`
--
ALTER TABLE `tamanio`
  ADD PRIMARY KEY (`tamanio_id`),
  ADD KEY `tamanio_animal_id` (`tamanio_animal_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `animal`
--
ALTER TABLE `animal`
  MODIFY `animal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `area_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `box`
--
ALTER TABLE `box`
  MODIFY `box_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `cliente_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `departamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `mascota`
--
ALTER TABLE `mascota`
  MODIFY `mascota_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `provincia_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `raza`
--
ALTER TABLE `raza`
  MODIFY `raza_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `reserva_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `reset`
--
ALTER TABLE `reset`
  MODIFY `reset_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `tamanio`
--
ALTER TABLE `tamanio`
  MODIFY `tamanio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `mascota`
--
ALTER TABLE `mascota`
  ADD CONSTRAINT `raza_animal_fk` FOREIGN KEY (`mascota_raza_id`) REFERENCES `raza` (`raza_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tamanio_raza_fk` FOREIGN KEY (`mascota_tamanio_id`) REFERENCES `tamanio` (`tamanio_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `raza`
--
ALTER TABLE `raza`
  ADD CONSTRAINT `raza_id_animal_fk` FOREIGN KEY (`raza_id_animal`) REFERENCES `animal` (`animal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `reserva_box_id_pk` FOREIGN KEY (`reserva_box_id`) REFERENCES `box` (`box_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `reserva_mascota_id_pk` FOREIGN KEY (`reserva_mascota_id`) REFERENCES `mascota` (`mascota_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tamanio`
--
ALTER TABLE `tamanio`
  ADD CONSTRAINT `tamanio_animal_fk` FOREIGN KEY (`tamanio_animal_id`) REFERENCES `animal` (`animal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
